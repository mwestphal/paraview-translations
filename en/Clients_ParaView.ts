<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1">
<context>
    <name>pqClientMainWindow</name>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="17"/>
        <source>MainWindow</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="53"/>
        <source>&amp;File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="58"/>
        <source>&amp;Sources</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="63"/>
        <source>Fi&amp;lters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="68"/>
        <source>&amp;Edit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="73"/>
        <source>&amp;View</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="78"/>
        <source>&amp;Tools</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="83"/>
        <source>&amp;Help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="88"/>
        <source>&amp;Macros</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="93"/>
        <source>&amp;Catalyst</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="98"/>
        <source>E&amp;xtractors</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="118"/>
        <source>Pipeline Browser</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="134"/>
        <source>Statistics Inspector</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="355"/>
        <source>Time Manager</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="146"/>
        <source>Comparative View Inspector</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="158"/>
        <source>Collaboration Panel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="170"/>
        <source>Information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="205"/>
        <source>Memory Inspector</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="223"/>
        <source>Properties</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="235"/>
        <source>MultiBlock Inspector</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="247"/>
        <source>Light Inspector</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="259"/>
        <source>Color Map Editor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="271"/>
        <source>Selection Editor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="292"/>
        <source>Material Editor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="308"/>
        <source>OSPRay support not available!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="323"/>
        <source>Display</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="339"/>
        <source>View</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="367"/>
        <source>Output Messages</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="374"/>
        <source>OutputMessages</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="389"/>
        <source>Python Shell</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="405"/>
        <source>Python support not available!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="420"/>
        <source>Find Data</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
