<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1">
<context>
    <name>BackgroundEditorWidget</name>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqBackgroundEditorWidget.ui" line="24"/>
        <source>Single color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqBackgroundEditorWidget.ui" line="29"/>
        <source>Gradient</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqBackgroundEditorWidget.ui" line="34"/>
        <source>Image</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqBackgroundEditorWidget.ui" line="39"/>
        <source>Skybox</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqBackgroundEditorWidget.ui" line="60"/>
        <source>Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqBackgroundEditorWidget.ui" line="79"/>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqBackgroundEditorWidget.ui" line="117"/>
        <source>Restore Default</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqBackgroundEditorWidget.ui" line="92"/>
        <source>Color 2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqBackgroundEditorWidget.ui" line="141"/>
        <source>Use As Environment Lighting</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>BoxPropertyWidget</name>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqBoxPropertyWidget.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqBoxPropertyWidget.ui" line="35"/>
        <source>Show the interactive box widget in the 3d scene.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqBoxPropertyWidget.ui" line="38"/>
        <source>Show Box</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqBoxPropertyWidget.ui" line="65"/>
        <source>Rotate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqBoxPropertyWidget.ui" line="75"/>
        <source>Translate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqBoxPropertyWidget.ui" line="106"/>
        <source>Scale</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqBoxPropertyWidget.ui" line="120"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Reference Bounds&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqBoxPropertyWidget.ui" line="136"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Use reference bounding box. When checked, &lt;span style=&quot; font-weight:600;&quot;&gt;Position&lt;/span&gt;, &lt;span style=&quot; font-weight:600;&quot;&gt;Rotation&lt;/span&gt;, and &lt;span style=&quot; font-weight:600;&quot;&gt;Scale&lt;/span&gt; are specified relative to the explicitly provided reference bounding box.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqBoxPropertyWidget.ui" line="139"/>
        <source>Use Reference Bounds</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqBoxPropertyWidget.ui" line="151"/>
        <source>Minimum X</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqBoxPropertyWidget.ui" line="158"/>
        <source>Maximum X</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqBoxPropertyWidget.ui" line="165"/>
        <source>Minimum Y</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqBoxPropertyWidget.ui" line="172"/>
        <source>Maximum Y</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqBoxPropertyWidget.ui" line="179"/>
        <source>Minimum Z</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqBoxPropertyWidget.ui" line="186"/>
        <source>Maximum Z</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqBoxPropertyWidget.ui" line="200"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Interactivity Controls&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqBoxPropertyWidget.ui" line="218"/>
        <source>Enable rotation of the 3d box widget.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqBoxPropertyWidget.ui" line="221"/>
        <source>Rotation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqBoxPropertyWidget.ui" line="228"/>
        <source>Enable translation of the 3d box widget.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqBoxPropertyWidget.ui" line="231"/>
        <source>Translation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqBoxPropertyWidget.ui" line="238"/>
        <source>Enable scaling of the 3d box widget.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqBoxPropertyWidget.ui" line="241"/>
        <source>Scaling</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqBoxPropertyWidget.ui" line="248"/>
        <source>Enable moving faces of the 3d box widget.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqBoxPropertyWidget.ui" line="251"/>
        <source>Face Movement</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqBoxPropertyWidget.ui" line="260"/>
        <source>Reset box using current data bounds</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqBoxPropertyWidget.ui" line="263"/>
        <source>Reset Bounds</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqBoxPropertyWidget.ui" line="270"/>
        <source>Take account of block visibility</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CameraManipulatorWidget</name>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCameraManipulatorWidget.ui" line="20"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCameraManipulatorWidget.ui" line="29"/>
        <source>Left Button</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCameraManipulatorWidget.ui" line="39"/>
        <source>Middle Button</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCameraManipulatorWidget.ui" line="49"/>
        <source>Right Button</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCameraManipulatorWidget.ui" line="68"/>
        <source>Shift +</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCameraManipulatorWidget.ui" line="87"/>
        <source>Ctrl +</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ColorAnnotationsWidget</name>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorAnnotationsWidget.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorAnnotationsWidget.ui" line="22"/>
        <source>Add new entry</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorAnnotationsWidget.ui" line="25"/>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorAnnotationsWidget.ui" line="39"/>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorAnnotationsWidget.ui" line="53"/>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorAnnotationsWidget.ui" line="67"/>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorAnnotationsWidget.ui" line="136"/>
        <source>...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorAnnotationsWidget.ui" line="36"/>
        <source>Remove current entry</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorAnnotationsWidget.ui" line="50"/>
        <source>Add active values from selected source</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorAnnotationsWidget.ui" line="64"/>
        <source>Add active values from visible pipeline objects</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorAnnotationsWidget.ui" line="78"/>
        <source>Choose preset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorAnnotationsWidget.ui" line="92"/>
        <source>Save to preset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorAnnotationsWidget.ui" line="106"/>
        <source>Save to new preset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorAnnotationsWidget.ui" line="133"/>
        <source>Delete all annotations</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorAnnotationsWidget.ui" line="171"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;When checked, the opacity function is used to render translucent surfaces.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorAnnotationsWidget.ui" line="174"/>
        <source>Enable opacity mapping for surfaces</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ColorEditorPropertyWidget</name>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorEditorPropertyWidget.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorEditorPropertyWidget.ui" line="20"/>
        <source>Rescale to custom range</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorEditorPropertyWidget.ui" line="37"/>
        <source>Edit color map</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorEditorPropertyWidget.ui" line="40"/>
        <source>Edit Color Map</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorEditorPropertyWidget.ui" line="43"/>
        <source>Edit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorEditorPropertyWidget.ui" line="57"/>
        <source>Use Separate color map</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorEditorPropertyWidget.ui" line="60"/>
        <source>Use Separate Color Map</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorEditorPropertyWidget.ui" line="77"/>
        <source>Rescale to data range</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorEditorPropertyWidget.ui" line="80"/>
        <source>Rescale to Data Range</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorEditorPropertyWidget.ui" line="97"/>
        <source>Show/hide color legend</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorEditorPropertyWidget.ui" line="100"/>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorEditorPropertyWidget.ui" line="151"/>
        <source>Toggle Color Legend Visibility</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorEditorPropertyWidget.ui" line="120"/>
        <source>Edit color legend properties</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorEditorPropertyWidget.ui" line="134"/>
        <source>Rescale to data range over all timesteps</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorEditorPropertyWidget.ui" line="148"/>
        <source>Choose preset</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ColorMapEditor</name>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorMapEditor.ui" line="26"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorMapEditor.ui" line="47"/>
        <source>Show/hide color legend</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorMapEditor.ui" line="64"/>
        <source>Edit color legend properties</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorMapEditor.ui" line="67"/>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorMapEditor.ui" line="170"/>
        <source>...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorMapEditor.ui" line="103"/>
        <source>Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorMapEditor.ui" line="110"/>
        <source>Color Y</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorMapEditor.ui" line="117"/>
        <source>Opacity</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorMapEditor.ui" line="124"/>
        <source>Use separate color map</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorMapEditor.ui" line="147"/>
        <source>Use a 2D transfer function. Available only for volume rendering.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorMapEditor.ui" line="167"/>
        <source>Use a separate array to map opacity. Available only for volume rendering.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorMapEditor.ui" line="190"/>
        <source>Select array used to map color.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorMapEditor.ui" line="197"/>
        <source>Select array used to map to the Y-Axis of a 2D transfer function.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorMapEditor.ui" line="204"/>
        <source>Select array used to map opacity.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorMapEditor.ui" line="237"/>
        <source>Restore application default setting values for color map</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorMapEditor.ui" line="254"/>
        <source>Save current color map settings values as default for arrays of this name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorMapEditor.ui" line="271"/>
        <source>Save current color map settings values as default for all arrays</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorMapEditor.ui" line="298"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Update views.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorMapEditor.ui" line="301"/>
        <source>Render Views</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorMapEditor.ui" line="315"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Render view(s) automatically.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ColorOpacityEditorWidget</name>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorOpacityEditorWidget.ui" line="20"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorOpacityEditorWidget.ui" line="42"/>
        <source>Automatically recompute data histogram</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorOpacityEditorWidget.ui" line="83"/>
        <source>Select a color map from default presets</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorOpacityEditorWidget.ui" line="214"/>
        <source>Select control point and press &quot;Enter&quot; or &quot;Return&quot; to change colors</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorOpacityEditorWidget.ui" line="284"/>
        <source>Rescale to data range</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorOpacityEditorWidget.ui" line="298"/>
        <source>Rescale to custom range</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorOpacityEditorWidget.ui" line="312"/>
        <source>Rescale to data range over all timesteps</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorOpacityEditorWidget.ui" line="326"/>
        <source>Rescale to visible range</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorOpacityEditorWidget.ui" line="340"/>
        <source>Invert the transfer functions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorOpacityEditorWidget.ui" line="354"/>
        <source>Choose preset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorOpacityEditorWidget.ui" line="368"/>
        <source>Save to preset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorOpacityEditorWidget.ui" line="382"/>
        <source>Choose box color and alpha</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorOpacityEditorWidget.ui" line="396"/>
        <source>Compute data histogram</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorOpacityEditorWidget.ui" line="92"/>
        <source>Data:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorOpacityEditorWidget.ui" line="105"/>
        <source>Set the data value for the selected control point</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorOpacityEditorWidget.ui" line="146"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;When checked, the control point of the opacity transfer fonction can be &quot;drawn&quot; instead of being placed one by one.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorOpacityEditorWidget.ui" line="149"/>
        <source>Enable Freehand Drawing Of Opacity Transfer Function</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorOpacityEditorWidget.ui" line="221"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;When checked, the mapping from data to colors is done using a log-scale. Note that this does not affect the mapping of data to opacity.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorOpacityEditorWidget.ui" line="224"/>
        <source>Use Log Scale When Mapping Data To Colors</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorOpacityEditorWidget.ui" line="156"/>
        <source>Enable Opacity Mapping For Surfaces</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorOpacityEditorWidget.ui" line="166"/>
        <source>Use Log Scale When Mapping Data To Opacity</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorOpacityEditorWidget.ui" line="237"/>
        <source>Color transfer function values</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorOpacityEditorWidget.ui" line="173"/>
        <source>Opacity transfer function values</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorOpacityEditorWidget.ui" line="29"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;When checked, a data histogram will be shown below the opacity function. Please update it by clicking on its dedicated button when needed.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorOpacityEditorWidget.ui" line="32"/>
        <source>Display Data Histogram</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorOpacityEditorWidget.ui" line="39"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;When checked, the data histogram will be automatically recomputed when needed instead of needing to click on the compute data histogram button.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorOpacityEditorWidget.ui" line="51"/>
        <source>Number of Bins:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CustomResolutionDialog</name>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCustomResolutionDialog.ui" line="14"/>
        <source>Custom Resolution</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCustomResolutionDialog.ui" line="20"/>
        <source>Label</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCustomResolutionDialog.ui" line="32"/>
        <source>width</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCustomResolutionDialog.ui" line="39"/>
        <source>x</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCustomResolutionDialog.ui" line="46"/>
        <source>height</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCustomResolutionDialog.ui" line="55"/>
        <source>Resolution</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCustomResolutionDialog.ui" line="85"/>
        <source>(optional)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCustomResolutionDialog.ui" line="92"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Add custom resolution&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CylinderPropertyWidget</name>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCylinderPropertyWidget.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCylinderPropertyWidget.ui" line="26"/>
        <source>Enable/disable showing the interactive cylinder widget in the 3d render view.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCylinderPropertyWidget.ui" line="29"/>
        <source>Show Cylinder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCylinderPropertyWidget.ui" line="44"/>
        <source>Enable/disable the ability to translate the bounding box by moving it with the mouse.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCylinderPropertyWidget.ui" line="47"/>
        <source>Outline Translation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCylinderPropertyWidget.ui" line="54"/>
        <source>Enable/disable the ability to scale the widget with the mouse.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCylinderPropertyWidget.ui" line="57"/>
        <source>Scaling</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCylinderPropertyWidget.ui" line="74"/>
        <source>Radius</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCylinderPropertyWidget.ui" line="84"/>
        <source>Axis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCylinderPropertyWidget.ui" line="104"/>
        <source>Center</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCylinderPropertyWidget.ui" line="121"/>
        <source>Use the X-axis as the cylinder&apos;s axis.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCylinderPropertyWidget.ui" line="124"/>
        <source>Along X Axis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCylinderPropertyWidget.ui" line="131"/>
        <source>Use the Y-axis as the cylinder&apos;s axis.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCylinderPropertyWidget.ui" line="134"/>
        <source>Along Y Axis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCylinderPropertyWidget.ui" line="141"/>
        <source>Use the Z-axis as the cylinder&apos;s axis.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCylinderPropertyWidget.ui" line="144"/>
        <source>Along Z Axis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCylinderPropertyWidget.ui" line="151"/>
        <source>Use the camera&apos;s view direction as the axis of the cylinder.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCylinderPropertyWidget.ui" line="154"/>
        <source>Along Camera Axis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCylinderPropertyWidget.ui" line="161"/>
        <source>Reset the camera to look along the cylinder&apos;s axis.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCylinderPropertyWidget.ui" line="164"/>
        <source>Reset Camera to Axis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCylinderPropertyWidget.ui" line="171"/>
        <source>Reset the cylinder&apos;s properties based on the data bounds.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCylinderPropertyWidget.ui" line="174"/>
        <source>Reset to Data Bounds</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DataAssemblyPropertyWidget</name>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqDataAssemblyPropertyWidget.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqDataAssemblyPropertyWidget.ui" line="39"/>
        <source>Hierarchy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqDataAssemblyPropertyWidget.ui" line="77"/>
        <source>Selectors</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqDataAssemblyPropertyWidget.ui" line="125"/>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqDataAssemblyPropertyWidget.ui" line="234"/>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqDataAssemblyPropertyWidget.ui" line="343"/>
        <source>Add new entry</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqDataAssemblyPropertyWidget.ui" line="128"/>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqDataAssemblyPropertyWidget.ui" line="145"/>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqDataAssemblyPropertyWidget.ui" line="172"/>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqDataAssemblyPropertyWidget.ui" line="237"/>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqDataAssemblyPropertyWidget.ui" line="254"/>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqDataAssemblyPropertyWidget.ui" line="281"/>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqDataAssemblyPropertyWidget.ui" line="346"/>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqDataAssemblyPropertyWidget.ui" line="363"/>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqDataAssemblyPropertyWidget.ui" line="390"/>
        <source>...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqDataAssemblyPropertyWidget.ui" line="142"/>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqDataAssemblyPropertyWidget.ui" line="251"/>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqDataAssemblyPropertyWidget.ui" line="360"/>
        <source>Remove current entry</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqDataAssemblyPropertyWidget.ui" line="169"/>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqDataAssemblyPropertyWidget.ui" line="278"/>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqDataAssemblyPropertyWidget.ui" line="387"/>
        <source>Remove all entries</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqDataAssemblyPropertyWidget.ui" line="186"/>
        <source>Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqDataAssemblyPropertyWidget.ui" line="295"/>
        <source>Opacity</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqDataAssemblyPropertyWidget.ui" line="407"/>
        <source>Select active assembly to use.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DoubleRangeSliderPropertyWidget</name>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqDoubleRangeSliderPropertyWidget.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqDoubleRangeSliderPropertyWidget.ui" line="26"/>
        <source>Minimum</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqDoubleRangeSliderPropertyWidget.ui" line="39"/>
        <source>Maximum</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqDoubleRangeSliderPropertyWidget.ui" line="56"/>
        <source>Reset using current data values</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>EmptyView</name>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqEmptyView.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqEmptyView.ui" line="76"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Helvetica&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p align=&quot;center&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Create View&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FileListPropertyWidget</name>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqFileListPropertyWidget.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqFileListPropertyWidget.ui" line="40"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Property Label&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqFileListPropertyWidget.ui" line="61"/>
        <source>Add new entry</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqFileListPropertyWidget.ui" line="64"/>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqFileListPropertyWidget.ui" line="81"/>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqFileListPropertyWidget.ui" line="95"/>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqFileListPropertyWidget.ui" line="118"/>
        <source>...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqFileListPropertyWidget.ui" line="78"/>
        <source>Remove current entry</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqFileListPropertyWidget.ui" line="115"/>
        <source>Remove all entries</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FindDataWidget</name>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqFindDataWidget.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqFindDataWidget.ui" line="20"/>
        <source>Create Selection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqFindDataWidget.ui" line="65"/>
        <source>Find data using selection criteria</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqFindDataWidget.ui" line="68"/>
        <source>Find Data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqFindDataWidget.ui" line="85"/>
        <source>Reset any unaccepted changes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqFindDataWidget.ui" line="88"/>
        <source>Reset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqFindDataWidget.ui" line="102"/>
        <source>Clear selection criteria and qualifiers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqFindDataWidget.ui" line="105"/>
        <source>Clear</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqFindDataWidget.ui" line="124"/>
        <source>Selected Data (none)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqFindDataWidget.ui" line="152"/>
        <source>Freeze</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqFindDataWidget.ui" line="162"/>
        <source>Extract</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqFindDataWidget.ui" line="172"/>
        <source>Plot Over Time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqFindDataWidget.ui" line="187"/>
        <source>Selection Display</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FontPropertyWidget</name>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqFontPropertyWidget.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqFontPropertyWidget.ui" line="40"/>
        <source>Select font</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqFontPropertyWidget.ui" line="53"/>
        <source>Set font size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqFontPropertyWidget.ui" line="63"/>
        <source>Set font color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqFontPropertyWidget.ui" line="73"/>
        <source>Set font opacity</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqFontPropertyWidget.ui" line="86"/>
        <source>Bold</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqFontPropertyWidget.ui" line="89"/>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqFontPropertyWidget.ui" line="106"/>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqFontPropertyWidget.ui" line="123"/>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqFontPropertyWidget.ui" line="137"/>
        <source>...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqFontPropertyWidget.ui" line="103"/>
        <source>Italics</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqFontPropertyWidget.ui" line="120"/>
        <source>Shadow</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqFontPropertyWidget.ui" line="164"/>
        <source>Specify the path to a TTF file here.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>HandlePropertyWidget</name>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqHandlePropertyWidget.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqHandlePropertyWidget.ui" line="38"/>
        <source>Show Point</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqHandlePropertyWidget.ui" line="51"/>
        <source>Center on Bounds</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqHandlePropertyWidget.ui" line="64"/>
        <source>Note: Use &apos;P&apos; to pick &apos;%1&apos; on mesh or &apos;Ctrl+P&apos; to snap to the closest mesh point</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqHandlePropertyWidget.ui" line="77"/>
        <source>Center on Selection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqHandlePropertyWidget.ui" line="84"/>
        <source>Point</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ImageCompressorWidget</name>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqImageCompressorWidget.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqImageCompressorWidget.ui" line="23"/>
        <source>Set the compression method used when transferring rendered images from the server to the client.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqImageCompressorWidget.ui" line="34"/>
        <source>None</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqImageCompressorWidget.ui" line="39"/>
        <source>LZ4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqImageCompressorWidget.ui" line="44"/>
        <source>Squirt (run-length encoding based compression)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqImageCompressorWidget.ui" line="49"/>
        <source>Zlib</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqImageCompressorWidget.ui" line="57"/>
        <source>Set the Squirt/LZ4 compression level. Move to right for better compression ratio at the cost of reduced image quality.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqImageCompressorWidget.ui" line="80"/>
        <source>Set the Zlib compression level. 1 is the fastest compression possible at the cost of compression ratio, while 9 give the best compression possible, but may be slower.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqImageCompressorWidget.ui" line="103"/>
        <source>Set the Zlib the color sampling space width factor. Move to right for better compression ratio at the cost of image quality.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqImageCompressorWidget.ui" line="126"/>
        <source>Set whether to strip alpha channel.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqImageCompressorWidget.ui" line="133"/>
        <source>Set the NvPipe compression level. 1 is high image quality; 5 is low image quality. Even at the highest setting the bandwidth cost is well below LZ4 or Squirt.
      </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqImageCompressorWidget.ui" line="159"/>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqImageCompressorWidget.ui" line="213"/>
        <source>Configure the compressor using default settings tailored to a selected connection type.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqImageCompressorWidget.ui" line="162"/>
        <source>Apply presets for</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqImageCompressorWidget.ui" line="172"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;DejaVu Sans&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Presets. &lt;/span&gt;Configure the image compressor based on a network type. These settings will provide reasonable performance and place to start when optimizing the compressor setting for a specific network.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqImageCompressorWidget.ui" line="180"/>
        <source>...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqImageCompressorWidget.ui" line="185"/>
        <source>consumer broadband/DSL</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqImageCompressorWidget.ui" line="190"/>
        <source>Megabit Ethernet / 802.11* wireless</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqImageCompressorWidget.ui" line="195"/>
        <source>Gigabit Ethernet</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqImageCompressorWidget.ui" line="200"/>
        <source>10 Gigabit Ethernet</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqImageCompressorWidget.ui" line="205"/>
        <source>shared memory/localhost</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqImageCompressorWidget.ui" line="216"/>
        <source>connection.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ImplicitPlanePropertyWidget</name>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqImplicitPlanePropertyWidget.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqImplicitPlanePropertyWidget.ui" line="26"/>
        <source>Show the interactive plane in the 3D render view.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqImplicitPlanePropertyWidget.ui" line="29"/>
        <source>Show Plane</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqImplicitPlanePropertyWidget.ui" line="47"/>
        <source>Normal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqImplicitPlanePropertyWidget.ui" line="57"/>
        <source>Origin</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqImplicitPlanePropertyWidget.ui" line="87"/>
        <source>Note: Use &apos;P&apos; to pick &apos;%1&apos; on mesh or &apos;Ctrl+P&apos; to snap to the closest mesh point</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqImplicitPlanePropertyWidget.ui" line="105"/>
        <source>Set the normal to the camera&apos;s view direction.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqImplicitPlanePropertyWidget.ui" line="108"/>
        <source>Camera Normal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqImplicitPlanePropertyWidget.ui" line="115"/>
        <source>Set the normal to the x-axis.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqImplicitPlanePropertyWidget.ui" line="118"/>
        <source>&amp;X Normal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqImplicitPlanePropertyWidget.ui" line="125"/>
        <source>Set the normal to the z-axis.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqImplicitPlanePropertyWidget.ui" line="128"/>
        <source>&amp;Z Normal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqImplicitPlanePropertyWidget.ui" line="135"/>
        <source>Set the normal to the y-axis.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqImplicitPlanePropertyWidget.ui" line="138"/>
        <source>&amp;Y Normal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqImplicitPlanePropertyWidget.ui" line="145"/>
        <source>Reset the plane based on the data bounds.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqImplicitPlanePropertyWidget.ui" line="148"/>
        <source>Reset to Data Bounds</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqImplicitPlanePropertyWidget.ui" line="155"/>
        <source>Reset the camera to look along the normal.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqImplicitPlanePropertyWidget.ui" line="158"/>
        <source>Reset Camera to Normal</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LightPropertyWidget</name>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqLightPropertyWidget.ui" line="32"/>
        <source>Focal Point</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqLightPropertyWidget.ui" line="45"/>
        <source>Light Position</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqLightPropertyWidget.ui" line="58"/>
        <source>Show Light</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqLightPropertyWidget.ui" line="68"/>
        <source>Cone Angle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqLightPropertyWidget.ui" line="87"/>
        <source>Type</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LinePropertyWidget</name>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqLinePropertyWidget.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqLinePropertyWidget.ui" line="26"/>
        <source>Point 1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqLinePropertyWidget.ui" line="45"/>
        <source>Point 2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqLinePropertyWidget.ui" line="64"/>
        <source>Flip Vector</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqLinePropertyWidget.ui" line="77"/>
        <source>Note: Use &apos;P&apos; to place alternating points on mesh or &apos;Ctrl+P&apos; to snap to the closest mesh point.
Use &apos;1&apos;/&apos;Ctrl+1&apos; for point 1 and &apos;2&apos;/&apos;Ctrl+2&apos; for point 2.
Use &apos;N&apos; to place P1 on the mesh and make P1-P2 be the normal at the surface.
Use &apos;X&apos;/&apos;Y&apos;/&apos;Z&apos;/&apos;L&apos; to constrain the movement to the X / Y / Z / Line axis respectively.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqLinePropertyWidget.ui" line="98"/>
        <source>X Axis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqLinePropertyWidget.ui" line="105"/>
        <source>Y Axis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqLinePropertyWidget.ui" line="112"/>
        <source>Z Axis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqLinePropertyWidget.ui" line="121"/>
        <source>Center on Bounds</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqLinePropertyWidget.ui" line="128"/>
        <source>Show Line</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqLinePropertyWidget.ui" line="138"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Length: &lt;/span&gt;&lt;span style=&quot; font-style:italic;&quot;&gt;na&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqDefaultMainWindow.ui" line="14"/>
        <source>ParaView - The Beast  UNLEASHED !!!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqDefaultMainWindow.ui" line="28"/>
        <source>&amp;File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqDefaultMainWindow.ui" line="34"/>
        <source>&amp;Help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqDefaultMainWindow.ui" line="44"/>
        <source>E&amp;xit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqDefaultMainWindow.ui" line="47"/>
        <source>Ctrl+Q</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqDefaultMainWindow.ui" line="52"/>
        <source>&amp;About</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MoleculePropertyWidget</name>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqMoleculePropertyWidget.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqMoleculePropertyWidget.ui" line="20"/>
        <source>Preset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqMoleculePropertyWidget.ui" line="30"/>
        <source>Show Atoms </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqMoleculePropertyWidget.ui" line="47"/>
        <source>Atom Radius type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqMoleculePropertyWidget.ui" line="57"/>
        <source>Atom Radius Factor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqMoleculePropertyWidget.ui" line="73"/>
        <source>Atomic Radius Array</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqMoleculePropertyWidget.ui" line="90"/>
        <source>Show Bonds</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqMoleculePropertyWidget.ui" line="107"/>
        <source>Bonds Radius</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqMoleculePropertyWidget.ui" line="123"/>
        <source>Use Multi Cylinders</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqMoleculePropertyWidget.ui" line="140"/>
        <source>Use Atom Color </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqMoleculePropertyWidget.ui" line="157"/>
        <source>Bonds color</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>OMETransferFunctionsPropertyWidget</name>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqOMETransferFunctionsPropertyWidget.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>OMETransferFunctionsPropertyWidgetPage</name>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqOMETransferFunctionsPropertyWidgetPage.ui" line="20"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqOMETransferFunctionsPropertyWidgetPage.ui" line="103"/>
        <source>Invert the transfer functions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqOMETransferFunctionsPropertyWidgetPage.ui" line="117"/>
        <source>Choose preset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqOMETransferFunctionsPropertyWidgetPage.ui" line="137"/>
        <source>Weight</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PropertyCollectionWidget</name>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqPropertyCollectionWidget.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqPropertyCollectionWidget.ui" line="45"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Group Label&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqPropertyCollectionWidget.ui" line="52"/>
        <source>Append new</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqPropertyCollectionWidget.ui" line="55"/>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqPropertyCollectionWidget.ui" line="69"/>
        <source>...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqPropertyCollectionWidget.ui" line="66"/>
        <source>Remove all</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqPropertyCollectionWidget.cxx" line="136"/>
        <source>Remove</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ResetScalarRangeToDataOverTime</name>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqResetScalarRangeToDataOverTime.ui" line="14"/>
        <source>Rescale range over time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqResetScalarRangeToDataOverTime.ui" line="23"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Determining range over all timesteps can potentially take a long time to complete. Are you sure you want to continue?&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqResetScalarRangeToDataOverTime.ui" line="57"/>
        <source>Rescale and lock the color map to avoid automatic rescaling.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqResetScalarRangeToDataOverTime.ui" line="60"/>
        <source>Rescale and disable automatic rescaling</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqResetScalarRangeToDataOverTime.ui" line="70"/>
        <source>Rescale and leave automatic rescaling mode unchanged.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqResetScalarRangeToDataOverTime.ui" line="73"/>
        <source>Rescale</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqResetScalarRangeToDataOverTime.ui" line="80"/>
        <source>Close without rescaling</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqResetScalarRangeToDataOverTime.ui" line="83"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SavePresetOptions</name>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSavePresetOptions.ui" line="14"/>
        <source>Save Preset Options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSavePresetOptions.ui" line="20"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Check to save colors to the preset.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSavePresetOptions.ui" line="23"/>
        <source>Save colors</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSavePresetOptions.ui" line="30"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Check to save opacities to the preset.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSavePresetOptions.ui" line="33"/>
        <source>Save opacities</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSavePresetOptions.ui" line="40"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Check to save annotations to the preset.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSavePresetOptions.ui" line="43"/>
        <source>Save annotations</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSavePresetOptions.ui" line="52"/>
        <source>Preset name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSavePresetOptions.ui" line="59"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Set a name to the preset.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSavePresetOptions.ui" line="62"/>
        <source>Preset</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SelectionEditor</name>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSelectionEditor.ui" line="20"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSelectionEditor.ui" line="37"/>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSelectionEditor.ui" line="47"/>
        <source>The dataset for which selections are saved</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSelectionEditor.ui" line="40"/>
        <source>Data Producer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSelectionEditor.ui" line="76"/>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSelectionEditor.ui" line="86"/>
        <source>The element type of the saved selections</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSelectionEditor.ui" line="79"/>
        <source>Element Type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSelectionEditor.ui" line="109"/>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSelectionEditor.ui" line="119"/>
        <source>Specify the expression which defines the relation between the saved selections using boolean operators: !(NOT), &amp;(AND), |(OR), ^(XOR) and ().</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSelectionEditor.ui" line="112"/>
        <source>Expression</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSelectionEditor.ui" line="169"/>
        <source>Add active selection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSelectionEditor.ui" line="186"/>
        <source>Remove selected selection from the saved selections. Remember to edit the Expression.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSelectionEditor.ui" line="216"/>
        <source>Remove all saved selections</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSelectionEditor.ui" line="244"/>
        <source>Set the combined saved selections as the active selection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSelectionEditor.ui" line="247"/>
        <source>Activate Combined Selections</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SeriesEditorPropertyWidget</name>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSeriesEditorPropertyWidget.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSeriesEditorPropertyWidget.ui" line="32"/>
        <source>Enter the thickness for the line.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSeriesEditorPropertyWidget.ui" line="45"/>
        <source>Line Thickness</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSeriesEditorPropertyWidget.ui" line="61"/>
        <source>Select the line style for the series.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSeriesEditorPropertyWidget.ui" line="65"/>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSeriesEditorPropertyWidget.ui" line="122"/>
        <source>None</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSeriesEditorPropertyWidget.ui" line="70"/>
        <source>Solid</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSeriesEditorPropertyWidget.ui" line="75"/>
        <source>Dash</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSeriesEditorPropertyWidget.ui" line="80"/>
        <source>Dot</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSeriesEditorPropertyWidget.ui" line="85"/>
        <source>Dash Dot</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSeriesEditorPropertyWidget.ui" line="90"/>
        <source>Dash Dot Dot</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSeriesEditorPropertyWidget.ui" line="98"/>
        <source>Line Style</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSeriesEditorPropertyWidget.ui" line="105"/>
        <source>Marker Style</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSeriesEditorPropertyWidget.ui" line="127"/>
        <source>Cross</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSeriesEditorPropertyWidget.ui" line="132"/>
        <source>Plus</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSeriesEditorPropertyWidget.ui" line="137"/>
        <source>Square</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSeriesEditorPropertyWidget.ui" line="142"/>
        <source>Circle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSeriesEditorPropertyWidget.ui" line="147"/>
        <source>Diamond</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSeriesEditorPropertyWidget.ui" line="164"/>
        <source>Enter the size for the marker.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSeriesEditorPropertyWidget.ui" line="180"/>
        <source>Marker Size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSeriesEditorPropertyWidget.ui" line="187"/>
        <source>Chart Axes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSeriesEditorPropertyWidget.ui" line="203"/>
        <source>Select the chart axes for the line series.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSeriesEditorPropertyWidget.ui" line="207"/>
        <source>Bottom-Left</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSeriesEditorPropertyWidget.ui" line="212"/>
        <source>Bottom-Right</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSeriesEditorPropertyWidget.ui" line="217"/>
        <source>Top-Right</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSeriesEditorPropertyWidget.ui" line="222"/>
        <source>Top-Left</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SpherePropertyWidget</name>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSpherePropertyWidget.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSpherePropertyWidget.ui" line="26"/>
        <source>Enable/disable showing the interactive sphere widget in the 3D render view.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSpherePropertyWidget.ui" line="29"/>
        <source>Show Sphere</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSpherePropertyWidget.ui" line="39"/>
        <source>Center</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSpherePropertyWidget.ui" line="55"/>
        <source>Normal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSpherePropertyWidget.ui" line="71"/>
        <source>Radius</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSpherePropertyWidget.ui" line="87"/>
        <source>Note: Use &apos;P&apos; to a &apos;%1&apos; on mesh or &apos;Ctrl+P&apos; to snap to the closest mesh point</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSpherePropertyWidget.ui" line="100"/>
        <source>Center on Bounds</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SplinePropertyWidget</name>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSplinePropertyWidget.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSplinePropertyWidget.ui" line="35"/>
        <source>Show Spline</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSplinePropertyWidget.ui" line="65"/>
        <source>Add new point</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSplinePropertyWidget.ui" line="68"/>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSplinePropertyWidget.ui" line="82"/>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSplinePropertyWidget.ui" line="109"/>
        <source>...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSplinePropertyWidget.ui" line="79"/>
        <source>Remove selected point(s)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSplinePropertyWidget.ui" line="106"/>
        <source>Remove all points</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSplinePropertyWidget.ui" line="124"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Use &lt;span style=&quot; font-weight:600;&quot;&gt;P&lt;/span&gt; to place selected point on mesh or &lt;span style=&quot; font-weight:600;&quot;&gt;Ctrl+P&lt;/span&gt; to snap the selected point to closest mesh point. Use &lt;span style=&quot; font-weight:600;&quot;&gt;1 &lt;/span&gt;/ &lt;span style=&quot; font-weight:600;&quot;&gt;Ctrl+1&lt;/span&gt; for first point and &lt;span style=&quot; font-weight:600;&quot;&gt;2 &lt;/span&gt;/ &lt;span style=&quot; font-weight:600;&quot;&gt;Ctrl+2&lt;/span&gt; for the last point.&lt;br/&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Click&lt;/span&gt; to select a point, &lt;span style=&quot; font-weight:600;&quot;&gt;Shift+Click&lt;/span&gt; to remove a point, &lt;span style=&quot; font-weight:600;&quot;&gt;Ctrl+Click&lt;/span&gt; to insert a point on the line, &lt;span style=&quot; font-weight:600;&quot;&gt;Alt+Click&lt;/span&gt; to add a point after the selected extremity.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSplinePropertyWidget.ui" line="134"/>
        <source>Closed Spline</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TextLocationWidget</name>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqTextLocationWidget.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqTextLocationWidget.ui" line="20"/>
        <source>Coordinates of the bottom left corner of the text object</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqTextLocationWidget.ui" line="23"/>
        <source>Use Coordinates</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqTextLocationWidget.ui" line="63"/>
        <source>Top Left Corner</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqTextLocationWidget.ui" line="101"/>
        <source>Bottom Right Corner</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqTextLocationWidget.ui" line="136"/>
        <source>Bottom Left Corner</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqTextLocationWidget.ui" line="171"/>
        <source>Top Right Corner</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqTextLocationWidget.ui" line="206"/>
        <source>Top Center</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqTextLocationWidget.ui" line="241"/>
        <source>Bottom Center</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqTextLocationWidget.ui" line="279"/>
        <source>Coordinates of the lower left corner of the text object</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqTextLocationWidget.ui" line="309"/>
        <source>Use Window Location</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TimeInspectorWidget</name>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqTimeInspectorWidget.ui" line="26"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TimeManagerWidget</name>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqTimeManagerWidget.ui" line="12"/>
        <source>TimeManager</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqTimeManagerWidget.ui" line="75"/>
        <source>Number of frames</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqTimeManagerWidget.ui" line="116"/>
        <source>Stride</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqTimeManagerWidget.ui" line="152"/>
        <source>...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TransferFunctionWidgetPropertyDialog</name>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqTransferFunctionWidgetPropertyDialog.ui" line="14"/>
        <source>Edit Transfer Function</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqTransferFunctionWidgetPropertyDialog.ui" line="20"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;%1&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ViewResolutionPropertyWidget</name>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqViewResolutionPropertyWidget.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqViewResolutionPropertyWidget.ui" line="26"/>
        <source>width</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqViewResolutionPropertyWidget.ui" line="33"/>
        <source>x</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqViewResolutionPropertyWidget.ui" line="40"/>
        <source>height</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqViewResolutionPropertyWidget.ui" line="47"/>
        <source>Select resolution from presets</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqViewResolutionPropertyWidget.ui" line="50"/>
        <source>Presets</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqViewResolutionPropertyWidget.ui" line="61"/>
        <source>Use previous resolution</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqViewResolutionPropertyWidget.ui" line="84"/>
        <source>Lock aspect ratio</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqViewResolutionPropertyWidget.ui" line="104"/>
        <source>Reset to default based on current values</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>YoungsMaterialPropertyWidget</name>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqYoungsMaterialPropertyWidget.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqYoungsMaterialPropertyWidget.ui" line="26"/>
        <source>Ordering Array</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqYoungsMaterialPropertyWidget.ui" line="33"/>
        <source>Normal Array</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqAnglePropertyWidget</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqAnglePropertyWidget.cxx" line="108"/>
        <source>1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqAnglePropertyWidget.cxx" line="112"/>
        <source>Ctrl+1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqAnglePropertyWidget.cxx" line="124"/>
        <source>C</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqAnglePropertyWidget.cxx" line="128"/>
        <source>Ctrl+C</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqAnglePropertyWidget.cxx" line="139"/>
        <source>2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqAnglePropertyWidget.cxx" line="143"/>
        <source>Ctrl+2</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqAnimatedExportReaction</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqAnimatedExportReaction.cxx" line="88"/>
        <source>Export Animated Scene ...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqAnimatedExportReaction.cxx" line="116"/>
        <source>Export animated scene progress</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqAnimatedExportReaction.cxx" line="116"/>
        <source>Abort</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqAnimatedExportReaction.cxx" line="118"/>
        <source>Saving animated scene ...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqAnimationShortcutWidget</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqAnimationShortcutWidget.cxx" line="116"/>
        <source>Create a new animation track</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqAnimationShortcutWidget.cxx" line="124"/>
        <source>Edit the animation track</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqAnimationShortcutWidget.cxx" line="128"/>
        <source>Remove the animation track</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqAnimationShortcutWidget.cxx" line="146"/>
        <source>Remove Animation Track</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqAnimationShortcutWidget.cxx" line="157"/>
        <source>Add Animation Track</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqAnimationShortcutWidget.cxx" line="172"/>
        <source>Animation Keyframes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqAnimationShortcutWidget.cxx" line="182"/>
        <source>No. frames:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqAnimationShortcutWidget.cxx" line="188"/>
        <source>Duration (s):</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqAnimationTimeToolbar</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqAnimationTimeToolbar.cxx" line="45"/>
        <source>Current Time Controls</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqApplyBehavior</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqApplyBehavior.cxx" line="286"/>
        <source>update the view to ensure updated data information</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqAxesToolbar</name>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqAxesToolbar.ui" line="9"/>
        <source>Center Axes Controls</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqAxesToolbar.ui" line="32"/>
        <source>Show Orientation Axes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqAxesToolbar.ui" line="35"/>
        <source>Show/Hide orientation axes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqAxesToolbar.ui" line="50"/>
        <source>Show Center</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqAxesToolbar.ui" line="53"/>
        <source>Show/Hide center of rotation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqAxesToolbar.ui" line="67"/>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqAxesToolbar.ui" line="70"/>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqAxesToolbar.ui" line="73"/>
        <source>Pick Center</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqAxesToolbar.ui" line="76"/>
        <source>Mouse press to pick center of rotation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqAxesToolbar.ui" line="87"/>
        <source>Reset Center</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqAxesToolbar.ui" line="90"/>
        <source>Reset center of rotation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqAxesToolbar.cxx" line="140"/>
        <source> Show orientation axes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqAxesToolbar.cxx" line="140"/>
        <source> Hide orientation axes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqAxesToolbar.cxx" line="156"/>
        <source> Show center axes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqAxesToolbar.cxx" line="156"/>
        <source> Hide center axes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqAxesToolbar.cxx" line="174"/>
        <location filename="../Qt/ApplicationComponents/pqAxesToolbar.cxx" line="200"/>
        <source> update center of rotation</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqBackgroundEditorWidget</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqBackgroundEditorWidget.cxx" line="230"/>
        <source>Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqBackgroundEditorWidget.cxx" line="230"/>
        <source>Color 1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqBackgroundEditorWidget.cxx" line="356"/>
        <source>Restore Default Color</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqBlockContextMenu</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqBlockContextMenu.cxx" line="97"/>
        <source>Block &apos;%1&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqBlockContextMenu.cxx" line="98"/>
        <source>%1 Blocks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqBlockContextMenu.cxx" line="109"/>
        <location filename="../Qt/ApplicationComponents/pqBlockContextMenu.cxx" line="166"/>
        <source>Hide Block</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqBlockContextMenu.cxx" line="174"/>
        <location filename="../Qt/ApplicationComponents/pqBlockContextMenu.cxx" line="177"/>
        <source>Show Only Block</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqBlockContextMenu.cxx" line="184"/>
        <location filename="../Qt/ApplicationComponents/pqBlockContextMenu.cxx" line="187"/>
        <source>Show All Blocks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqBlockContextMenu.cxx" line="198"/>
        <location filename="../Qt/ApplicationComponents/pqBlockContextMenu.cxx" line="208"/>
        <source>Set Block Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqBlockContextMenu.cxx" line="218"/>
        <location filename="../Qt/ApplicationComponents/pqBlockContextMenu.cxx" line="220"/>
        <source>Unset Block Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqBlockContextMenu.cxx" line="235"/>
        <location filename="../Qt/ApplicationComponents/pqBlockContextMenu.cxx" line="238"/>
        <location filename="../Qt/ApplicationComponents/pqBlockContextMenu.cxx" line="245"/>
        <source>Set Block Opacity</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqBlockContextMenu.cxx" line="236"/>
        <source>Opacity:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqBlockContextMenu.cxx" line="257"/>
        <location filename="../Qt/ApplicationComponents/pqBlockContextMenu.cxx" line="259"/>
        <source>Unset Block Opacity</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqCameraToolbar</name>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCameraToolbar.ui" line="14"/>
        <source>Camera Controls</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCameraToolbar.ui" line="28"/>
        <source>&amp;Reset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCameraToolbar.ui" line="31"/>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCameraToolbar.ui" line="34"/>
        <source>Reset Camera</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCameraToolbar.ui" line="46"/>
        <source>+X</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCameraToolbar.ui" line="49"/>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCameraToolbar.ui" line="52"/>
        <source>Set view direction to +X</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCameraToolbar.ui" line="64"/>
        <source>-X</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCameraToolbar.ui" line="67"/>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCameraToolbar.ui" line="70"/>
        <source>Set view direction to -X</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCameraToolbar.ui" line="82"/>
        <source>+Y</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCameraToolbar.ui" line="85"/>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCameraToolbar.ui" line="88"/>
        <source>Set view direction to +Y</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCameraToolbar.ui" line="100"/>
        <source>-Y</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCameraToolbar.ui" line="103"/>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCameraToolbar.ui" line="106"/>
        <source>Set view direction to -Y</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCameraToolbar.ui" line="118"/>
        <source>+Z</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCameraToolbar.ui" line="121"/>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCameraToolbar.ui" line="124"/>
        <source>Set view direction to +Z</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCameraToolbar.ui" line="136"/>
        <source>-Z</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCameraToolbar.ui" line="139"/>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCameraToolbar.ui" line="142"/>
        <source>Set view direction to -Z</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCameraToolbar.ui" line="154"/>
        <source>&amp;Isometric</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCameraToolbar.ui" line="157"/>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCameraToolbar.ui" line="160"/>
        <source>Apply isometric view</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCameraToolbar.ui" line="175"/>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCameraToolbar.ui" line="178"/>
        <source>Rotate 90° clockwise</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCameraToolbar.ui" line="193"/>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCameraToolbar.ui" line="196"/>
        <source>Rotate 90° counterclockwise</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCameraToolbar.ui" line="208"/>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCameraToolbar.ui" line="211"/>
        <source>Zoom to Box</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCameraToolbar.ui" line="216"/>
        <source>ZTD</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCameraToolbar.ui" line="219"/>
        <source>Zoom To Data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCameraToolbar.ui" line="228"/>
        <source>ZCTD</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCameraToolbar.ui" line="231"/>
        <source>Zoom Closest To Data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCameraToolbar.ui" line="240"/>
        <source>RCC</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCameraToolbar.ui" line="243"/>
        <source>Reset Camera Closest</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqCatalystExportReaction</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqCatalystExportReaction.cxx" line="61"/>
        <location filename="../Qt/ApplicationComponents/pqCatalystExportReaction.cxx" line="62"/>
        <source>Needs Python support</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqCatalystExportReaction.cxx" line="72"/>
        <source>Save Catalyst State:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqCatalystExportReaction.cxx" line="73"/>
        <source>Python file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqCatalystExportReaction.cxx" line="73"/>
        <source>All files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqCatalystExportReaction.cxx" line="99"/>
        <source>Save Catalyst State Options</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqCatalystMenu</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="906"/>
        <source>Connect...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="909"/>
        <source>Pause Simulation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="913"/>
        <source>Continue</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="917"/>
        <source>Set Breakpoint</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="921"/>
        <source>Remove Breakpoint</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqChangeFileNameReaction</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqChangeFileNameReaction.cxx" line="123"/>
        <source>Open File:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqChangePipelineInputReaction</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqChangePipelineInputReaction.cxx" line="95"/>
        <source>Change Input for %1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqChooseColorPresetReaction</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqChooseColorPresetReaction.cxx" line="178"/>
        <source>Apply color preset</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqColorAnnotationsPropertyWidget</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqColorAnnotationsPropertyWidget.cxx" line="206"/>
        <source>Could not initialize annotations for categorical coloring. There may be too many discrete values in your data, (more than %1) or you may be coloring by a floating point data array. Please add annotations manually.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqColorAnnotationsPropertyWidget.cxx" line="212"/>
        <source>Could not determine discrete values to use for annotations</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqColorAnnotationsSelectionHelper</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqColorAnnotationsWidget.cxx" line="287"/>
        <source>Set opacity of highlited items</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqColorAnnotationsWidget.cxx" line="312"/>
        <source>Set opacity of highlighted items</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqColorAnnotationsWidget</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqColorAnnotationsWidget.cxx" line="561"/>
        <source>Set global opacity</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqColorAnnotationsWidget.cxx" line="582"/>
        <source>Choose Annotation Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqColorAnnotationsWidget.cxx" line="591"/>
        <source>Opacity:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqColorAnnotationsWidget.cxx" line="592"/>
        <source>Select Opacity</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqColorAnnotationsWidget.cxx" line="773"/>
        <location filename="../Qt/ApplicationComponents/pqColorAnnotationsWidget.cxx" line="858"/>
        <source>Could not determine discrete values</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqColorAnnotationsWidget.cxx" line="776"/>
        <location filename="../Qt/ApplicationComponents/pqColorAnnotationsWidget.cxx" line="861"/>
        <source>Could not automatically determine annotation values. Usually this means too many discrete values (more than %1) are available in the data produced by the current source/filter.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqColorAnnotationsWidget.cxx" line="781"/>
        <location filename="../Qt/ApplicationComponents/pqColorAnnotationsWidget.cxx" line="866"/>
        <source> This can happen if the data array type is floating point. Please add annotations manually or force generation. Forcing the generation will automatically hide the Scalar Bar.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqColorAnnotationsWidget.cxx" line="787"/>
        <location filename="../Qt/ApplicationComponents/pqColorAnnotationsWidget.cxx" line="873"/>
        <source>Force</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqColorAnnotationsWidget.cxx" line="798"/>
        <location filename="../Qt/ApplicationComponents/pqColorAnnotationsWidget.cxx" line="884"/>
        <source>Could not force generation of discrete values using the data produced by the current source/filter. Please add annotations manually.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqColorAnnotationsWidget.cxx" line="1019"/>
        <source>Save %1s</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqColorAnnotationsWidget.cxx" line="1026"/>
        <source>Preset</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqColorMapEditor</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqColorMapEditor.cxx" line="357"/>
        <source>Color X</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqColorMapEditor.cxx" line="361"/>
        <source>Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqColorMapEditor.cxx" line="476"/>
        <source>Reset color map to defaults</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqColorOpacityEditorWidget</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqColorOpacityEditorWidget.cxx" line="1090"/>
        <source>Reset transfer function ranges using visible data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqColorOpacityEditorWidget.cxx" line="1123"/>
        <source>Invert transfer function</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqColorOpacityEditorWidget.cxx" line="1160"/>
        <source>Select a color map from default presets</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqColorOpacityEditorWidget.cxx" line="1568"/>
        <source>Choose box color</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqColorPaletteSelectorWidget</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqColorPaletteSelectorWidget.cxx" line="103"/>
        <source>Select palette to load ...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqColorPaletteSelectorWidget.cxx" line="107"/>
        <source>No palettes available.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqColorToolbar</name>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorToolbar.ui" line="6"/>
        <source>Active Variable Controls</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorToolbar.ui" line="28"/>
        <source>Show Color Legend</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorToolbar.ui" line="31"/>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorToolbar.ui" line="34"/>
        <source>Toggle Color Legend Visibility</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorToolbar.ui" line="45"/>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorToolbar.ui" line="48"/>
        <source>Edit Color Map</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorToolbar.ui" line="59"/>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorToolbar.ui" line="62"/>
        <source>Use Separate Color Map</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorToolbar.ui" line="73"/>
        <source>Reset Range</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorToolbar.ui" line="76"/>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorToolbar.ui" line="79"/>
        <source>Rescale to Data Range</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorToolbar.ui" line="90"/>
        <source>Rescale Custom Range</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorToolbar.ui" line="93"/>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorToolbar.ui" line="96"/>
        <source>Rescale to Custom Data Range</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorToolbar.ui" line="108"/>
        <source>Rescale Temporal Range</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorToolbar.ui" line="111"/>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorToolbar.ui" line="114"/>
        <source>Rescale to data range over all timesteps</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorToolbar.ui" line="125"/>
        <source>Rescale Visible Range</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorToolbar.ui" line="128"/>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorToolbar.ui" line="131"/>
        <source>Rescale to Visible Data Range</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqCommandLineOptionsBehavior</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqCommandLineOptionsBehavior.cxx" line="202"/>
        <source>Internal Open File</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqCoordinateFramePropertyWidget</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqCoordinateFramePropertyWidget.cxx" line="239"/>
        <source>Coordinate frame utilities</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqCoordinateFramePropertyWidget.cxx" line="258"/>
        <source>P</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqCoordinateFramePropertyWidget.cxx" line="264"/>
        <source>Ctrl+P</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqCoordinateFramePropertyWidget.cxx" line="271"/>
        <source>N</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqCoordinateFramePropertyWidget.cxx" line="277"/>
        <source>Ctrl+N</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqCoordinateFramePropertyWidget.cxx" line="284"/>
        <source>T</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqCoordinateFramePropertyWidget.cxx" line="290"/>
        <source>Ctrl+T</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqCopyReaction</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqCopyReaction.cxx" line="492"/>
        <source>Copy Properties</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqCrashRecoveryBehavior</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqCrashRecoveryBehavior.cxx" line="67"/>
        <source>ParaView</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqCrashRecoveryBehavior.cxx" line="68"/>
        <source>A crash recovery state file has been found.
Would you like to save it?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqCrashRecoveryBehavior.cxx" line="73"/>
        <source>ParaView state file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqCrashRecoveryBehavior.cxx" line="73"/>
        <source>All files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqCrashRecoveryBehavior.cxx" line="75"/>
        <source>Save crash state file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqCrashRecoveryBehavior.cxx" line="154"/>
        <source>Server disconnected!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqCrashRecoveryBehavior.cxx" line="155"/>
        <source>The server side has disconnected. The application will now quit since it may be in an unrecoverable state.

Would you like to save a ParaView state file?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqCrashRecoveryBehavior.cxx" line="159"/>
        <source>Save state and exit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqCrashRecoveryBehavior.cxx" line="160"/>
        <source>Exit without saving state</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqCreateCustomFilterReaction</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqCreateCustomFilterReaction.cxx" line="81"/>
        <source>Create Custom Filter Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqCreateCustomFilterReaction.cxx" line="82"/>
        <source>The selected objects cannot be used to make a custom filter.
To create a new custom filter, select the sources and filters you want.
Then, launch the creation wizard.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqCustomViewpointsToolbar</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqCustomViewpointsToolbar.cxx" line="61"/>
        <source>Custom Viewpoints Toolbar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqCustomViewpointsToolbar.cxx" line="88"/>
        <source>Configure custom viewpoints</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqCustomViewpointsToolbar.cxx" line="95"/>
        <source>Add current viewpoint as custom viewpoint</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqCustomizeShortcutsDialog</name>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCustomizeShortcutsDialog.ui" line="20"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCustomizeShortcutsDialog.ui" line="73"/>
        <source>To set the shortcut for the selected action, press this then enter the shortcut.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCustomizeShortcutsDialog.ui" line="76"/>
        <source>Record</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCustomizeShortcutsDialog.ui" line="89"/>
        <source>Remove the shortcut for the selected action.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCustomizeShortcutsDialog.ui" line="92"/>
        <source>Clear</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCustomizeShortcutsDialog.ui" line="99"/>
        <source>Restore the default shortcut for the selected action.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCustomizeShortcutsDialog.ui" line="102"/>
        <source>Reset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCustomizeShortcutsDialog.ui" line="119"/>
        <source>Resets the shortcuts for all actions to the default or clears the shortcut if there is no default.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCustomizeShortcutsDialog.ui" line="122"/>
        <source>Reset All</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqCustomizeShortcutsDialog.cxx" line="443"/>
        <source>Customize Shortcuts</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqDataAssemblyPropertyWidget</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqDataAssemblyPropertyWidget.cxx" line="1268"/>
        <source>Select Opacity</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqDefaultContextMenu</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqDefaultContextMenu.cxx" line="124"/>
        <source>Hide</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqDefaultContextMenu.cxx" line="127"/>
        <source>Representation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqDefaultContextMenu.cxx" line="150"/>
        <source>Color By</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqDefaultContextMenu.cxx" line="155"/>
        <source>Edit Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqDefaultContextMenu.cxx" line="162"/>
        <location filename="../Qt/ApplicationComponents/pqDefaultContextMenu.cxx" line="247"/>
        <source>Show All Blocks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqDefaultContextMenu.cxx" line="168"/>
        <source>Link Camera...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqDefaultContextMenu.cxx" line="173"/>
        <source>Show Frame Decorations</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqDefaultContextMenu.cxx" line="193"/>
        <source>Solid Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqDefaultContextMenu.cxx" line="262"/>
        <source>Change coloring</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqDefaultContextMenu.cxx" line="314"/>
        <source>Representation Type Changed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqDefaultContextMenu.cxx" line="330"/>
        <source>Visibility Changed</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqDefaultViewBehavior</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqDefaultViewBehavior.cxx" line="94"/>
        <source>Your OpenGL drivers don&apos;t support
required OpenGL features for basic rendering.
Application cannot continue. Please exit and use an older version.

CONTINUE AT YOUR OWN RISK!

</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqDefaultViewBehavior.cxx" line="100"/>
        <source>OpenGL support inadequate!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqDefaultViewBehavior.cxx" line="112"/>
        <source>Server DISPLAY not accessible!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqDefaultViewBehavior.cxx" line="113"/>
        <source>Display is not accessible on the server side.
Remote rendering will be disabled.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqDefaultViewBehavior.cxx" line="119"/>
        <source>OpenGL drivers on the server side don&apos;t support
required OpenGL features for basic rendering.
Remote rendering will be disabled.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqDefaultViewBehavior.cxx" line="124"/>
        <source>Server OpenGL support inadequate!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqDefaultViewBehavior.cxx" line="185"/>
        <location filename="../Qt/ApplicationComponents/pqDefaultViewBehavior.cxx" line="194"/>
        <source>Server Timeout Warning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqDefaultViewBehavior.cxx" line="186"/>
        <source>The server connection will timeout under 5 minutes.
Please save your work.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqDefaultViewBehavior.cxx" line="195"/>
        <source>The server connection will timeout shortly.
Please save your work.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqDeleteReaction</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqDeleteReaction.cxx" line="337"/>
        <source>Delete %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqDeleteReaction.cxx" line="341"/>
        <source>Delete Selection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqDeleteReaction.cxx" line="455"/>
        <source>Delete All?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqDeleteReaction.cxx" line="456"/>
        <source>The current visualization will be reset
and the state will be discarded.

Are you sure you want to continue?</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqDesktopServicesReaction</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqDesktopServicesReaction.cxx" line="63"/>
        <source>The requested file is not available in your installation. You can manually obtain and place the file (or ask your administrators) at the following location for this to work.

&apos;%1&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqDesktopServicesReaction.cxx" line="69"/>
        <source>Missing file</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqDisplaySizedImplicitPlanePropertyWidget</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqDisplaySizedImplicitPlanePropertyWidget.cxx" line="100"/>
        <source>Origin</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqDisplaySizedImplicitPlanePropertyWidget.cxx" line="167"/>
        <source>P</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqDisplaySizedImplicitPlanePropertyWidget.cxx" line="173"/>
        <source>Ctrl+P</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqDisplaySizedImplicitPlanePropertyWidget.cxx" line="180"/>
        <source>N</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqDisplaySizedImplicitPlanePropertyWidget.cxx" line="186"/>
        <source>Ctrl+N</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqEditCameraReaction</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqEditCameraReaction.cxx" line="82"/>
        <source>Adjust Camera</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqEditColorMapReaction</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqEditColorMapReaction.cxx" line="101"/>
        <source> change solid color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqEditColorMapReaction.cxx" line="118"/>
        <source>Pick Solid Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqEditColorMapReaction.cxx" line="125"/>
        <source>Changed Solid Color</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqEditMenu</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="240"/>
        <source>Apply</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="243"/>
        <source>Reset</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqEditMenuBuilder</name>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqEditMenuBuilder.ui" line="6"/>
        <source>&amp;Edit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqEditMenuBuilder.ui" line="20"/>
        <source>&amp;Undo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqEditMenuBuilder.ui" line="23"/>
        <source>Ctrl+Z</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqEditMenuBuilder.ui" line="35"/>
        <source>&amp;Redo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqEditMenuBuilder.ui" line="38"/>
        <source>Ctrl+Y</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqEditMenuBuilder.ui" line="47"/>
        <source>Camera Undo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqEditMenuBuilder.ui" line="50"/>
        <source>Ctrl+B</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqEditMenuBuilder.ui" line="59"/>
        <source>Camera Redo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqEditMenuBuilder.ui" line="62"/>
        <source>Ctrl+G</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqEditMenuBuilder.ui" line="67"/>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqEditMenuBuilder.ui" line="70"/>
        <source>Change File...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqEditMenuBuilder.ui" line="73"/>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqEditMenuBuilder.ui" line="76"/>
        <source>Change File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqEditMenuBuilder.ui" line="85"/>
        <source>Change &amp;Input...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqEditMenuBuilder.ui" line="88"/>
        <source>Change Input...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqEditMenuBuilder.ui" line="91"/>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqEditMenuBuilder.ui" line="94"/>
        <source>Change a Filter&apos;s Input</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqEditMenuBuilder.ui" line="99"/>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqEditMenuBuilder.ui" line="102"/>
        <source>Rename...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqEditMenuBuilder.ui" line="105"/>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqEditMenuBuilder.ui" line="108"/>
        <source>Rename selected source (or filter)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqEditMenuBuilder.ui" line="117"/>
        <source>&amp;Copy Screenshot to Clipboard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqEditMenuBuilder.ui" line="120"/>
        <source>Copy Screenshot to Clipboard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqEditMenuBuilder.ui" line="129"/>
        <source>&amp;Copy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqEditMenuBuilder.ui" line="132"/>
        <source>Copy properties</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqEditMenuBuilder.ui" line="141"/>
        <source>&amp;Paste</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqEditMenuBuilder.ui" line="144"/>
        <source>Paste copied properties</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqEditMenuBuilder.ui" line="153"/>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqEditMenuBuilder.ui" line="156"/>
        <source>Copy Pipeline</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqEditMenuBuilder.ui" line="165"/>
        <source>Paste Pipeline</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqEditMenuBuilder.ui" line="168"/>
        <source>Paste copied pipeline</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqEditMenuBuilder.ui" line="177"/>
        <source>&amp;Delete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqEditMenuBuilder.ui" line="180"/>
        <source>Delete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqEditMenuBuilder.ui" line="189"/>
        <source>Delete Downstream Pipeline</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqEditMenuBuilder.ui" line="192"/>
        <source>Delete selection and all downstream filters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqEditMenuBuilder.ui" line="201"/>
        <source>Reset Session</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqEditMenuBuilder.ui" line="204"/>
        <source>Ctrl+R</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqEditMenuBuilder.ui" line="213"/>
        <source>Show All</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqEditMenuBuilder.ui" line="216"/>
        <source>Show all sources in selected view</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqEditMenuBuilder.ui" line="225"/>
        <source>Hide All</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqEditMenuBuilder.ui" line="233"/>
        <source>Ignore Time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqEditMenuBuilder.ui" line="236"/>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqEditMenuBuilder.ui" line="239"/>
        <source>Disregard this source/filter&apos;s time from animations</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqEditMenuBuilder.ui" line="248"/>
        <source>Settings...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqEditMenuBuilder.ui" line="251"/>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqEditMenuBuilder.ui" line="254"/>
        <source>Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqEditMenuBuilder.ui" line="263"/>
        <source>Find Data...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqEditMenuBuilder.ui" line="266"/>
        <source>Find Data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqEditMenuBuilder.ui" line="269"/>
        <source>Find data matching various criteria from the current source.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqEditMenuBuilder.ui" line="272"/>
        <source>V</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqEditMenuBuilder.ui" line="277"/>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqEditMenuBuilder.ui" line="280"/>
        <source>Search in item list</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqEditMenuBuilder.ui" line="283"/>
        <source>Pops up a search dialog if focus is on an item list</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqEditMenuBuilder.ui" line="286"/>
        <source>Ctrl+F</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqEditMenuBuilder.ui" line="295"/>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqEditMenuBuilder.ui" line="298"/>
        <source>Reset to Default Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqEditMenuBuilder.ui" line="301"/>
        <source>Reset user settings to default.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqEditMenuBuilder.ui" line="310"/>
        <source>Rename Window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqEditMenuBuilder.ui" line="313"/>
        <source>Change the main window title</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqEditScalarBarReaction</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqEditScalarBarReaction.cxx" line="81"/>
        <source>Edit Color Legend Properties</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqEqualizerPropertyWidget</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqEqualizerPropertyWidget.cxx" line="76"/>
        <source>Visibility</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqEqualizerPropertyWidget.cxx" line="79"/>
        <source>Save</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqEqualizerPropertyWidget.cxx" line="80"/>
        <source>Load</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqEqualizerPropertyWidget.cxx" line="81"/>
        <source>Reset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqEqualizerPropertyWidget.cxx" line="169"/>
        <source>Save Equalizer:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqEqualizerPropertyWidget.cxx" line="194"/>
        <source>Load Equalizer:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqExampleVisualizationsDialog</name>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqExampleVisualizationsDialog.ui" line="14"/>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqExampleVisualizationsDialog.ui" line="68"/>
        <source>ParaView Example Visualizations</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqExampleVisualizationsDialog.ui" line="20"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;Click on one of thumbnails below to load an example visualization&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqExampleVisualizationsDialog.ui" line="47"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Data analysis of hot gas from Exodus II file&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqExampleVisualizationsDialog.ui" line="78"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Wavelet with volume rendering and contours&lt;br/&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqExampleVisualizationsDialog.ui" line="91"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Contouring CT scan of a head&lt;br/&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqExampleVisualizationsDialog.ui" line="104"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Sea surface temperatures&lt;br/&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqExampleVisualizationsDialog.ui" line="127"/>
        <source>Exodus II file, Clip filter, Stream Tracer filter, Tube filter, Glyph filter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqExampleVisualizationsDialog.ui" line="202"/>
        <source>Exodus II file with timesteps, Clip filter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqExampleVisualizationsDialog.cxx" line="95"/>
        <source>Your installation doesn&apos;t have datasets to load the example visualizations. You can manually download the datasets from paraview.org and then place them under the following path for examples to work:

&apos;%1&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqExampleVisualizationsDialog.cxx" line="101"/>
        <source>Missing data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqExampleVisualizationsDialog.cxx" line="114"/>
        <source>Loading example visualization, please wait ...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqExportReaction</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqExportReaction.cxx" line="136"/>
        <source>Export View:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqExportReaction.cxx" line="158"/>
        <source>Export Options</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqExtractorsMenuReaction</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqExtractorsMenuReaction.cxx" line="136"/>
        <location filename="../Qt/ApplicationComponents/pqExtractorsMenuReaction.cxx" line="166"/>
        <source>Requires an input</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqExtractorsMenuReaction.cxx" line="204"/>
        <source>Create Extract Generator &apos;%1&apos;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqFileListPropertyWidget</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqFileListPropertyWidget.cxx" line="75"/>
        <source>Select %1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqFileMenuBuilder</name>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqFileMenuBuilder.ui" line="6"/>
        <source>&amp;File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqFileMenuBuilder.ui" line="14"/>
        <source>&amp;Recent Files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqFileMenuBuilder.ui" line="23"/>
        <source>&amp;Connect...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqFileMenuBuilder.ui" line="26"/>
        <source>Connect</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqFileMenuBuilder.ui" line="35"/>
        <source>&amp;Disconnect</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqFileMenuBuilder.ui" line="38"/>
        <source>Disconnect</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqFileMenuBuilder.ui" line="47"/>
        <source>Save Screenshot...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqFileMenuBuilder.ui" line="56"/>
        <source>Save &amp;Animation...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqFileMenuBuilder.ui" line="68"/>
        <source>Save Data...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqFileMenuBuilder.ui" line="71"/>
        <source>Ctrl+S</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqFileMenuBuilder.ui" line="83"/>
        <source>&amp;Load State...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqFileMenuBuilder.ui" line="95"/>
        <source>&amp;Save State...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqFileMenuBuilder.ui" line="104"/>
        <source>Save Catalyst State...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqFileMenuBuilder.ui" line="109"/>
        <source>Save &amp;Geometry...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqFileMenuBuilder.ui" line="121"/>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqFileMenuBuilder.ui" line="124"/>
        <source>Reload Files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqFileMenuBuilder.ui" line="127"/>
        <source>Reload data files in case they were changed externally.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqFileMenuBuilder.ui" line="130"/>
        <source>F5</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqFileMenuBuilder.ui" line="139"/>
        <source>&amp;Open...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqFileMenuBuilder.ui" line="142"/>
        <source>Open</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqFileMenuBuilder.ui" line="145"/>
        <source>Ctrl+O</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqFileMenuBuilder.ui" line="157"/>
        <source>E&amp;xit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqFileMenuBuilder.ui" line="160"/>
        <source>Exit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqFileMenuBuilder.ui" line="163"/>
        <source>Ctrl+Q</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqFileMenuBuilder.ui" line="172"/>
        <source>Export Scene...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqFileMenuBuilder.ui" line="181"/>
        <source>Export Animated Scene...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqFileMenuBuilder.ui" line="184"/>
        <source>Save scene for all timesteps by playing the animation step by step</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqFileMenuBuilder.ui" line="187"/>
        <source>Save scene for all timesteps</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqFileMenuBuilder.ui" line="196"/>
        <source>Save Window Arrangement...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqFileMenuBuilder.ui" line="199"/>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqFileMenuBuilder.ui" line="202"/>
        <source>Save window arrangement to a file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqFileMenuBuilder.ui" line="211"/>
        <source>Load Window Arrangement...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqFileMenuBuilder.ui" line="214"/>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqFileMenuBuilder.ui" line="217"/>
        <source>Load window arrangement from a file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqFileMenuBuilder.ui" line="222"/>
        <source>Load Path Tracer Materials...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqFileMenuBuilder.ui" line="225"/>
        <source>Load a Materials file that contains appearance settings for use in high quality rendering modes.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqFileMenuBuilder.ui" line="228"/>
        <source>Load an Appearance Materials file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqFileMenuBuilder.ui" line="237"/>
        <source>Save Extracts...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqFileNamePropertyWidget</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqFileNamePropertyWidget.cxx" line="91"/>
        <source>Reset using current data values</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqFiltersMenuReaction</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqFiltersMenuReaction.cxx" line="202"/>
        <source>Requires an input</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqFiltersMenuReaction.cxx" line="221"/>
        <source>Not supported in parallel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqFiltersMenuReaction.cxx" line="225"/>
        <source>Supported only in parallel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqFiltersMenuReaction.cxx" line="241"/>
        <source>Multiple inputs not support</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqFiltersMenuReaction.cxx" line="391"/>
        <source>Create &apos;%1&apos;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqFindDataWidget</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqFindDataWidget.cxx" line="207"/>
        <source>Convert Selection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqFindDataWidget.cxx" line="208"/>
        <source>This selection conversion can potentially result in fetching a large amount of data to the client.
Are you sure you want to continue?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqFindDataWidget.cxx" line="342"/>
        <source>Selected Data (%1)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqFindDataWidget.cxx" line="346"/>
        <source>Selected Data (none)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqFontPropertyWidget</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqFontPropertyWidget.cxx" line="261"/>
        <source>Left</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqFontPropertyWidget.cxx" line="264"/>
        <location filename="../Qt/ApplicationComponents/pqFontPropertyWidget.cxx" line="288"/>
        <source>Center</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqFontPropertyWidget.cxx" line="267"/>
        <source>Right</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqFontPropertyWidget.cxx" line="285"/>
        <source>Top</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqFontPropertyWidget.cxx" line="291"/>
        <source>Bottom</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqHandlePropertyWidget</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqHandlePropertyWidget.cxx" line="67"/>
        <source>Point</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqHandlePropertyWidget.cxx" line="100"/>
        <source>P</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqHandlePropertyWidget.cxx" line="106"/>
        <source>Ctrl+P</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqHelpMenu</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="710"/>
        <source>Getting Started with ParaView</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="723"/>
        <source>ParaView Guide</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="741"/>
        <source>Reader, Filter, and Writer Reference</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="755"/>
        <source>ParaView Self-directed Tutorial</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="763"/>
        <source>ParaView Classroom Tutorials</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="770"/>
        <source>Example Visualizations</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="778"/>
        <source>ParaView Web Site</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="783"/>
        <source>ParaView Wiki</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="788"/>
        <source>ParaView Community Support</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="795"/>
        <source>Release Notes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="803"/>
        <source>Professional Support</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="808"/>
        <source>Professional Training</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="813"/>
        <source>Online Tutorials</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="818"/>
        <source>Online Blogs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="834"/>
        <source>Bug Report</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="838"/>
        <source>About...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqHelpReaction</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqHelpReaction.cxx" line="77"/>
        <source>%1 Online Help</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqIgnoreSourceTimeReaction</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqIgnoreSourceTimeReaction.cxx" line="88"/>
        <source>Toggle Ignore Time</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqImageCompressorWidget</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqImageCompressorWidget.cxx" line="66"/>
        <source>Image Compression</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqImplicitPlanePropertyWidget</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqImplicitPlanePropertyWidget.cxx" line="100"/>
        <source>Origin</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqImplicitPlanePropertyWidget.cxx" line="152"/>
        <source>P</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqImplicitPlanePropertyWidget.cxx" line="158"/>
        <source>Ctrl+P</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqLanguageChooserWidget</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqLanguageChooserWidget.cxx" line="106"/>
        <source>Setting overriden by environment variable PV_TRANSLATIONS_LOCALE</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqLightToolbar</name>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqLightToolbar.ui" line="9"/>
        <source>Light controls</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqLightToolbar.ui" line="32"/>
        <source>Enable/Disable Light Kit</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqLinePropertyWidget</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqLinePropertyWidget.cxx" line="115"/>
        <source>P</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqLinePropertyWidget.cxx" line="121"/>
        <source>Ctrl+P</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqLinePropertyWidget.cxx" line="126"/>
        <source>1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqLinePropertyWidget.cxx" line="132"/>
        <source>Ctrl+1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqLinePropertyWidget.cxx" line="137"/>
        <source>2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqLinePropertyWidget.cxx" line="143"/>
        <source>Ctrl+2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqLinePropertyWidget.cxx" line="149"/>
        <source>N</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqLoadDataReaction</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqLoadDataReaction.cxx" line="186"/>
        <source>Open File:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqLoadDataReaction.cxx" line="450"/>
        <source>Create &apos;Reader&apos;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqLoadMaterialsReaction</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqLoadMaterialsReaction.cxx" line="61"/>
        <source>Load Materials:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqLoadMaterialsReaction.cxx" line="62"/>
        <source>OSPRay Material Files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqLoadMaterialsReaction.cxx" line="62"/>
        <source>Wavefront Material Files</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqLoadPaletteReaction</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqLoadPaletteReaction.cxx" line="137"/>
        <source>Edit Current Palette ...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqLoadPaletteReaction.cxx" line="154"/>
        <source>Load color palette</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqLoadPaletteReaction.cxx" line="170"/>
        <source>Color Palette</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqLoadStateReaction</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqLoadStateReaction.cxx" line="104"/>
        <source>Load State Options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqLoadStateReaction.cxx" line="145"/>
        <source>Load State File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqLoadStateReaction.cxx" line="146"/>
        <source>ParaView state file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqLoadStateReaction.cxx" line="153"/>
        <source>All files</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqMacroReaction</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqMacroReaction.cxx" line="60"/>
        <source>Open Python File to create a Macro:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqMacroReaction.cxx" line="61"/>
        <source>Python Files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqMacroReaction.cxx" line="61"/>
        <source>All Files</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqMacrosMenu</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="666"/>
        <source>Import new macro...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="668"/>
        <source>Edit...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="670"/>
        <source>Delete...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="673"/>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="676"/>
        <source>Delete All</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="677"/>
        <source>All macros will be deleted. Are you sure?</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqMainControlsToolbar</name>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqMainControlsToolbar.ui" line="14"/>
        <source>Main Controls</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqMainControlsToolbar.ui" line="25"/>
        <source>Open...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqMainControlsToolbar.ui" line="28"/>
        <source>Open</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqMainControlsToolbar.ui" line="40"/>
        <source>Save Data...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqMainControlsToolbar.ui" line="52"/>
        <source>&amp;Connect...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqMainControlsToolbar.ui" line="55"/>
        <source>Connect</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqMainControlsToolbar.ui" line="64"/>
        <source>&amp;Disconnect</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqMainControlsToolbar.ui" line="67"/>
        <source>Disconnect</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqMainControlsToolbar.ui" line="76"/>
        <source>&amp;Undo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqMainControlsToolbar.ui" line="85"/>
        <source>&amp;Redo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqMainControlsToolbar.ui" line="97"/>
        <source>Auto Apply</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqMainControlsToolbar.ui" line="100"/>
        <source>Apply changes to parameters automatically</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqMainControlsToolbar.ui" line="115"/>
        <source>Find Data...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqMainControlsToolbar.ui" line="118"/>
        <source>Find data matching various criteria from the current source (v)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqMainControlsToolbar.ui" line="127"/>
        <source>Load Palette</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqMainControlsToolbar.ui" line="130"/>
        <source>Load a color palette</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqMainControlsToolbar.ui" line="139"/>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqMainControlsToolbar.ui" line="142"/>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqMainControlsToolbar.ui" line="145"/>
        <source>Reset Session</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqMainControlsToolbar.ui" line="154"/>
        <source>Save Extracts...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqMainControlsToolbar.ui" line="157"/>
        <source>Save Extracts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqMainControlsToolbar.ui" line="166"/>
        <source>Save State...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqMainControlsToolbar.ui" line="169"/>
        <source>Save State</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqMainControlsToolbar.ui" line="178"/>
        <source>Save Catalyst State...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqMainControlsToolbar.ui" line="181"/>
        <source>Save Catalyst State</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqMainWindowEventBehavior</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqMainWindowEventBehavior.cxx" line="123"/>
        <source>Read PNG or state file?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqMainWindowEventBehavior.cxx" line="124"/>
        <source>This PNG file has a ParaView state file embedded.
Do you want to open this file as a state file?</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqManageLinksReaction</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqManageLinksReaction.cxx" line="41"/>
        <source>Link Manager</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqMoleculePropertyWidget</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqMoleculePropertyWidget.cxx" line="88"/>
        <location filename="../Qt/ApplicationComponents/pqMoleculePropertyWidget.cxx" line="147"/>
        <source>Reset the range values</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqMoleculePropertyWidget.cxx" line="126"/>
        <source>Apply a preset to display properties, including advanced ones.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqOpacityRangeDialog</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqColorAnnotationsWidget.cxx" line="252"/>
        <source>Opacity value :</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqPipelineBrowserContextMenu</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="421"/>
        <source>&amp;Open</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="424"/>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="428"/>
        <source>Open</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="438"/>
        <source>&amp;Show All</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="440"/>
        <source>Show all source outputs in the pipeline</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="451"/>
        <source>&amp;Hide All</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="453"/>
        <source>Hide all source outputs in the pipeline</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="463"/>
        <source>&amp;Copy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="466"/>
        <source>Copy Properties</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="476"/>
        <source>&amp;Paste</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="479"/>
        <source>Paste Properties</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="486"/>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="489"/>
        <source>Copy Pipeline</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="496"/>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="499"/>
        <source>Paste Pipeline</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="505"/>
        <source>Change &amp;Input...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="507"/>
        <source>Change Input...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="509"/>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="513"/>
        <source>Change a Filter&apos;s Input</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="520"/>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="522"/>
        <source>Reload Files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="524"/>
        <source>Reload data files in case they were changed externally.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="531"/>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="534"/>
        <source>Change File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="541"/>
        <source>Ignore Time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="543"/>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="547"/>
        <source>Disregard this source/filter&apos;s time from animations</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="558"/>
        <source>&amp;Delete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="561"/>
        <source>Delete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="575"/>
        <source>Delete Downstream Pipeline</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="578"/>
        <source>Delete selection and all downstream filters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="584"/>
        <source>&amp;Create Custom Filter...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="590"/>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="592"/>
        <source>Link with selection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="594"/>
        <source>Link this source and current selected source as a selection link</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="601"/>
        <source>Rename</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="603"/>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="607"/>
        <source>Rename currently selected source</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="629"/>
        <source>Add Filter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqProxyGroupMenuManager.cxx" line="461"/>
        <source>&amp;Add current filter</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqPreviewMenuManager</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqPreviewMenuManager.cxx" line="127"/>
        <source>Custom ...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqPreviewMenuManager.cxx" line="305"/>
        <source>Requested resolution too big for window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqPreviewMenuManager.cxx" line="306"/>
        <source>The resolution requested is too big for the current window. Fitting to aspect ratio instead.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqProxyGroupMenuManager</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqProxyGroupMenuManager.cxx" line="403"/>
        <source>&amp;Manage Favorites...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqProxyGroupMenuManager.cxx" line="558"/>
        <source>Search...	Alt+Space</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqProxyGroupMenuManager.cxx" line="561"/>
        <source>Search...	Ctrl+Space</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqProxyGroupMenuManager.cxx" line="567"/>
        <source>&amp;Recent</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqProxyGroupMenuManager.cxx" line="574"/>
        <source>&amp;Favorites</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqProxyGroupMenuManager.cxx" line="585"/>
        <source>&amp;Alphabetical</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqReloadFilesReaction</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqReloadFilesReaction.cxx" line="61"/>
        <source>Reload Options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqReloadFilesReaction.cxx" line="62"/>
        <source>This reader supports file series. Do you want to look for new files in the series and load those, or reload the existing files?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqReloadFilesReaction.cxx" line="69"/>
        <source>Find new files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqReloadFilesReaction.cxx" line="73"/>
        <source>Reload existing file(s)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqRenameProxyReaction</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqRenameProxyReaction.cxx" line="83"/>
        <source>View</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqRenameProxyReaction.cxx" line="83"/>
        <source>Proxy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqRenameProxyReaction.cxx" line="87"/>
        <location filename="../Qt/ApplicationComponents/pqRenameProxyReaction.cxx" line="106"/>
        <source>Rename</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqRenameProxyReaction.cxx" line="87"/>
        <source>New name:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqRepresentationToolbar</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqRepresentationToolbar.cxx" line="41"/>
        <source>Representation Toolbar</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqResetDefaultSettingsReaction</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqResetDefaultSettingsReaction.cxx" line="59"/>
        <location filename="../Qt/ApplicationComponents/pqResetDefaultSettingsReaction.cxx" line="107"/>
        <source>Reset to Default Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqResetDefaultSettingsReaction.cxx" line="61"/>
        <source>Reset custom settings to default.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqResetDefaultSettingsReaction.cxx" line="63"/>
        <source>All custom setting will be reset to their default values. Do you want to continue?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqResetDefaultSettingsReaction.cxx" line="66"/>
        <source>Yes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqResetDefaultSettingsReaction.cxx" line="67"/>
        <source>Yes, and backup current settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqResetDefaultSettingsReaction.cxx" line="84"/>
        <source>Backups failed!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqResetDefaultSettingsReaction.cxx" line="85"/>
        <source>Failed to generate backup files. Do you want to continue?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqResetDefaultSettingsReaction.cxx" line="93"/>
        <source>Following backup files have been generated:

</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqResetDefaultSettingsReaction.cxx" line="94"/>
        <source>

</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqResetDefaultSettingsReaction.cxx" line="104"/>
        <source>Please restart %1 for the changes to take effect.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqResetDefaultSettingsReaction.cxx" line="109"/>
        <source>Settings reset to default</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqResetScalarRangeReaction</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqResetScalarRangeReaction.cxx" line="161"/>
        <source>Reset transfer function ranges using data range</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqResetScalarRangeReaction.cxx" line="235"/>
        <source>Reset transfer function ranges</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqResetScalarRangeReaction.cxx" line="303"/>
        <source>Reset transfer function ranges using temporal data range</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqResetScalarRangeReaction.cxx" line="345"/>
        <source>Reset transfer function ranges to visible data range</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqSaveAnimationGeometryReaction</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqSaveAnimationGeometryReaction.cxx" line="86"/>
        <source>Save Animation Geometry</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqSaveAnimationGeometryReaction.cxx" line="115"/>
        <source>Save geometry progress</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqSaveAnimationGeometryReaction.cxx" line="115"/>
        <source>Abort</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqSaveAnimationGeometryReaction.cxx" line="117"/>
        <source>Saving Geometry ...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqSaveAnimationReaction</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqSaveAnimationReaction.cxx" line="162"/>
        <source>Save Animation Options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqSaveAnimationReaction.cxx" line="169"/>
        <source>Save animation progress</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqSaveAnimationReaction.cxx" line="169"/>
        <source>Abort</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqSaveAnimationReaction.cxx" line="170"/>
        <source>Saving Animation ...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqSaveDataReaction</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqSaveDataReaction.cxx" line="117"/>
        <source>Save File:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqSaveDataReaction.cxx" line="159"/>
        <source>Serial Writer Warning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqSaveDataReaction.cxx" line="160"/>
        <source>This writer (%1) will collect all of the data to the first node before writing because it does not support parallel IO. This may cause the first node to run out of memory if the data is large.
Are you sure you want to continue?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqSaveDataReaction.cxx" line="177"/>
        <source>Configure Writer (%1)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqSaveExtractsReaction</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqSaveExtractsReaction.cxx" line="72"/>
        <source>Save Extracts Options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqSaveExtractsReaction.cxx" line="78"/>
        <source>Saving Extracts ...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqSaveScreenshotReaction</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqSaveScreenshotReaction.cxx" line="220"/>
        <source>Save Screenshot Options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqSaveScreenshotReaction.cxx" line="310"/>
        <source>View content has been copied to the clipboard.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqSaveStateReaction</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqSaveStateReaction.cxx" line="79"/>
        <location filename="../Qt/ApplicationComponents/pqSaveStateReaction.cxx" line="83"/>
        <source>ParaView state file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqSaveStateReaction.cxx" line="79"/>
        <source>Python state file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqSaveStateReaction.cxx" line="80"/>
        <location filename="../Qt/ApplicationComponents/pqSaveStateReaction.cxx" line="83"/>
        <source>All Files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqSaveStateReaction.cxx" line="86"/>
        <source>Save State File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqSaveStateReaction.cxx" line="140"/>
        <source>Python State Options</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqScalarBarVisibilityReaction</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqScalarBarVisibilityReaction.cxx" line="172"/>
        <source>Number of annotations warning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqScalarBarVisibilityReaction.cxx" line="173"/>
        <source>The color map have been configured to show lots of annotations. Showing the scalar bar in this situation may slow down the rendering and it may not be readable anyway. Do you really want to show the color map ?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqScalarBarVisibilityReaction.cxx" line="187"/>
        <source>Toggle Color Legend Visibility</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqSelectionEditor</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqSelectionEditor.cxx" line="480"/>
        <location filename="../Qt/ApplicationComponents/pqSelectionEditor.cxx" line="673"/>
        <source>none</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqSelectionEditor.cxx" line="835"/>
        <source>Different Element Type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqSelectionEditor.cxx" line="836"/>
        <source>The current active selection has a different element type compared to chosen element type.
Are you sure you want to continue?</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqSelectionListPropertyWidget</name>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSelectionListPropertyWidget.ui" line="21"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSelectionListPropertyWidget.ui" line="46"/>
        <source>Labels list</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqSeriesAnnotationsModel</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqSeriesEditorPropertyWidget.cxx" line="76"/>
        <source>Variable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqSeriesEditorPropertyWidget.cxx" line="76"/>
        <source>Series name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqSeriesEditorPropertyWidget.cxx" line="78"/>
        <source>Toggle series visibility</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqSeriesEditorPropertyWidget.cxx" line="80"/>
        <source>Set color to use for the series</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqSeriesEditorPropertyWidget.cxx" line="82"/>
        <source>Legend Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqSeriesEditorPropertyWidget.cxx" line="83"/>
        <source>Set the text to use for the series in the legend</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqServerConnectReaction</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqServerConnectReaction.cxx" line="79"/>
        <source>Disconnect from current server?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqServerConnectReaction.cxx" line="81"/>
        <source>Before connecting to a new server, the current connection will be closed and the state will be discarded. </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqServerConnectReaction.cxx" line="83"/>
        <source>Are you sure you want to continue?</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqServerDisconnectReaction</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqServerDisconnectReaction.cxx" line="78"/>
        <source>Disconnect from current server?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqServerDisconnectReaction.cxx" line="79"/>
        <source>The current connection will be closed and
the state will be discarded.

Are you sure you want to continue?</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqSetMainWindowTitleReaction</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqSetMainWindowTitleReaction.cxx" line="51"/>
        <source>Rename Window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqSetMainWindowTitleReaction.cxx" line="51"/>
        <source>New title:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqShowHideAllReaction</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqShowHideAllReaction.cxx" line="56"/>
        <source>Show All</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqShowHideAllReaction.cxx" line="63"/>
        <source>Hide All</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqSourcesMenuReaction</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqSourcesMenuReaction.cxx" line="196"/>
        <source>Are you sure?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqSourcesMenuReaction.cxx" line="200"/>
        <source>Creating &apos;%1&apos;. Do you want to continue?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqSourcesMenuReaction.cxx" line="220"/>
        <source>Create &apos;%1&apos;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqSpherePropertyWidget</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqSpherePropertyWidget.cxx" line="66"/>
        <source>Center</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqSpherePropertyWidget.cxx" line="116"/>
        <source>P</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqSpherePropertyWidget.cxx" line="122"/>
        <source>Ctrl+P</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqSplinePropertyWidget</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqSplinePropertyWidget.cxx" line="341"/>
        <source>P</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqSplinePropertyWidget.cxx" line="346"/>
        <source>Ctrl+P</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqSplinePropertyWidget.cxx" line="350"/>
        <source>1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqSplinePropertyWidget.cxx" line="357"/>
        <source>Ctrl+1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqSplinePropertyWidget.cxx" line="363"/>
        <source>2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqSplinePropertyWidget.cxx" line="370"/>
        <source>Ctrl+2</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqSpreadSheetViewDecorator</name>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSpreadSheetViewDecorator.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSpreadSheetViewDecorator.ui" line="35"/>
        <source>&lt;b&gt;Showing  &lt;/b&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSpreadSheetViewDecorator.ui" line="49"/>
        <source>&lt;b&gt;   Attribute:&lt;/b&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSpreadSheetViewDecorator.ui" line="69"/>
        <source>Precision:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSpreadSheetViewDecorator.ui" line="86"/>
        <source>Switches between scientific and fixed-point representation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSpreadSheetViewDecorator.ui" line="89"/>
        <source>Toggle fixed-point representation (always show #Precision digits)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSpreadSheetViewDecorator.ui" line="92"/>
        <source>FixedRep</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSpreadSheetViewDecorator.ui" line="106"/>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSpreadSheetViewDecorator.ui" line="109"/>
        <source>Show only selected elements.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSpreadSheetViewDecorator.ui" line="112"/>
        <source>Selected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSpreadSheetViewDecorator.ui" line="129"/>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSpreadSheetViewDecorator.ui" line="132"/>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSpreadSheetViewDecorator.ui" line="135"/>
        <source>Toggle column visibility</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSpreadSheetViewDecorator.ui" line="149"/>
        <source>Toggle cell connectivity visibility</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSpreadSheetViewDecorator.ui" line="152"/>
        <source>...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSpreadSheetViewDecorator.ui" line="186"/>
        <source>Export Spreadsheet</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqStandardViewFrameActionsImplementation</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqStandardViewFrameActionsImplementation.cxx" line="127"/>
        <source>s</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqStandardViewFrameActionsImplementation.cxx" line="128"/>
        <source>d</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqStandardViewFrameActionsImplementation.cxx" line="129"/>
        <source>f</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqStandardViewFrameActionsImplementation.cxx" line="130"/>
        <source>g</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqStandardViewFrameActionsImplementation.cxx" line="211"/>
        <source>Polygon Selection (d)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqStandardViewFrameActionsImplementation.cxx" line="224"/>
        <source>Rectangle Selection (s)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqStandardViewFrameActionsImplementation.cxx" line="258"/>
        <source>Add selection (Ctrl)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqStandardViewFrameActionsImplementation.cxx" line="267"/>
        <source>Subtract selection (Shift)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqStandardViewFrameActionsImplementation.cxx" line="276"/>
        <source>Toggle selection (Ctrl+Shift)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqStandardViewFrameActionsImplementation.cxx" line="323"/>
        <source>Rename</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqStandardViewFrameActionsImplementation.cxx" line="326"/>
        <source>Convert To ...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqStandardViewFrameActionsImplementation.cxx" line="336"/>
        <source>Camera Undo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqStandardViewFrameActionsImplementation.cxx" line="344"/>
        <source>Camera Redo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqStandardViewFrameActionsImplementation.cxx" line="357"/>
        <source>Capture to Clipboard or File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqStandardViewFrameActionsImplementation.cxx" line="359"/>
        <source>Capture screenshot to a file or to the clipboard if a modifier key (Ctrl, Alt or Shift) is pressed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqStandardViewFrameActionsImplementation.cxx" line="378"/>
        <source>Change Interaction Mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqStandardViewFrameActionsImplementation.cxx" line="386"/>
        <source>Adjust Camera</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqStandardViewFrameActionsImplementation.cxx" line="398"/>
        <source>Select Cells On (s)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqStandardViewFrameActionsImplementation.cxx" line="410"/>
        <source>Select Points On (d)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqStandardViewFrameActionsImplementation.cxx" line="422"/>
        <source>Select Cells Through (f)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqStandardViewFrameActionsImplementation.cxx" line="434"/>
        <source>Select Points Through (g)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqStandardViewFrameActionsImplementation.cxx" line="446"/>
        <source>Select Cells With Polygon</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqStandardViewFrameActionsImplementation.cxx" line="458"/>
        <source>Select Points With Polygon</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqStandardViewFrameActionsImplementation.cxx" line="470"/>
        <source>Select Block (b)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqStandardViewFrameActionsImplementation.cxx" line="482"/>
        <source>Interactive Select Cell Data On</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqStandardViewFrameActionsImplementation.cxx" line="497"/>
        <source>Interactive Select Point Data On</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqStandardViewFrameActionsImplementation.cxx" line="513"/>
        <source>Interactive Select Cells On</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqStandardViewFrameActionsImplementation.cxx" line="528"/>
        <source>Interactive Select Points On</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqStandardViewFrameActionsImplementation.cxx" line="543"/>
        <source>Hover Cells On. Use Ctrl-C/Cmd-C to copy the content to clipboard.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqStandardViewFrameActionsImplementation.cxx" line="558"/>
        <source>Hover Points On. Use Ctrl-C/Cmd-C to copy the content to clipboard.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqStandardViewFrameActionsImplementation.cxx" line="572"/>
        <source>Grow selection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqStandardViewFrameActionsImplementation.cxx" line="581"/>
        <source>Shrink selection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqStandardViewFrameActionsImplementation.cxx" line="591"/>
        <source>Clear selection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqStandardViewFrameActionsImplementation.cxx" line="711"/>
        <source>Convert To</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqStandardViewFrameActionsImplementation.cxx" line="733"/>
        <source>Create</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqTestingReaction</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqTestingReaction.cxx" line="61"/>
        <location filename="../Qt/ApplicationComponents/pqTestingReaction.cxx" line="90"/>
        <source>XML Files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqTestingReaction.cxx" line="63"/>
        <location filename="../Qt/ApplicationComponents/pqTestingReaction.cxx" line="92"/>
        <source>Python Files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqTestingReaction.cxx" line="65"/>
        <location filename="../Qt/ApplicationComponents/pqTestingReaction.cxx" line="94"/>
        <source>All Files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqTestingReaction.cxx" line="67"/>
        <source>Record Test</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqTestingReaction.cxx" line="96"/>
        <source>Play Test</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqTimelineItemDelegate</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqTimelineItemDelegate.cxx" line="79"/>
        <source>%1 is unlocked. Lock to avoid auto-update when adding/removing time sources.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqTimelineItemDelegate.cxx" line="85"/>
        <source>%1 is locked. Unlock to allow auto-update when adding/removing time sources.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqTimelineItemDelegate.cxx" line="95"/>
        <location filename="../Qt/ApplicationComponents/pqTimelineItemDelegate.cxx" line="102"/>
        <location filename="../Qt/ApplicationComponents/pqTimelineItemDelegate.cxx" line="107"/>
        <source>Start time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqTimelineItemDelegate.cxx" line="114"/>
        <source>End Time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqTimelineItemDelegate.cxx" line="121"/>
        <location filename="../Qt/ApplicationComponents/pqTimelineItemDelegate.cxx" line="126"/>
        <source>End time</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqTimelineView</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqTimelineView.cxx" line="67"/>
        <source>Reset Start and End Time to default values</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqTimelineView.cxx" line="86"/>
        <source>Select proxy to animate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqTimelineView.cxx" line="89"/>
        <source>Python</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqTimelineView.cxx" line="95"/>
        <source>Select property to animate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqTimelineView.cxx" line="108"/>
        <location filename="../Qt/ApplicationComponents/pqTimelineView.cxx" line="109"/>
        <location filename="../Qt/ApplicationComponents/pqTimelineView.cxx" line="111"/>
        <source>Camera</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqTimelineView.cxx" line="136"/>
        <source>Follow Path</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqTimelineView.cxx" line="138"/>
        <source>Follow Data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqTimelineView.cxx" line="140"/>
        <source>Interpolate cameras</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqTimelineView.cxx" line="284"/>
        <source>Add animation cue for selected proxy and property.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqTimelineView.cxx" line="288"/>
        <source>Animation cue already exists.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqTimelineWidget</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqTimelineWidget.cxx" line="141"/>
        <source>If checked, scene use times from time sources.
Otherwise, generate NumberOfFrames time entries.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqTimelineWidget.cxx" line="143"/>
        <source>Time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqTimelineWidget.cxx" line="148"/>
        <source>Check / Uncheck to enable all animation tracks.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqTimelineWidget.cxx" line="190"/>
        <source>Remove Animation Track</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqTimelineWidget.cxx" line="263"/>
        <source>Add Animation Track</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqTimelineWidget.cxx" line="340"/>
        <source>Check/Uncheck to make timesteps available in the scene time list.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqTimelineWidget.cxx" line="661"/>
        <source>Check / uncheck to enable the animation. Double click on name or timeline to edit.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqTimelineWidget.cxx" line="668"/>
        <source>Timekeeper updates pipeline. It maps scene time to a requested pipeline time.
 Uncheck to freeze pipeline time.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqToolsMenu</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="331"/>
        <source>Create Custom Filter...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="334"/>
        <source>Add Camera Link...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="337"/>
        <source>Link with Selection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="341"/>
        <source>Manage Custom Filters...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="344"/>
        <source>Manage Links...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="349"/>
        <source>Manage Plugins...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="357"/>
        <source>Manage Favorites...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="362"/>
        <source>Customize Shortcuts...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="366"/>
        <source>Manage Expressions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="372"/>
        <source>Record Test...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="375"/>
        <source>Play Test...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="378"/>
        <source>Lock View Size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="382"/>
        <source>Lock View Size Custom...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="386"/>
        <source>Timer Log</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="388"/>
        <source>Log Viewer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="393"/>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="395"/>
        <source>Start Trace</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="396"/>
        <source>Stop Trace</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="399"/>
        <source>Python Script Editor</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqTraceReaction</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqTraceReaction.cxx" line="77"/>
        <location filename="../Qt/ApplicationComponents/pqTraceReaction.cxx" line="79"/>
        <source>Tracing unavailable since application built without Python support.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqTraceReaction.cxx" line="120"/>
        <source>Trace Options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqTraceReaction.cxx" line="131"/>
        <source>Recording python trace...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqTransferFunctionWidgetPropertyWidget</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqTransferFunctionWidgetPropertyWidget.cxx" line="103"/>
        <source>Reset using current data values</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqUndoRedoReaction</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqUndoRedoReaction.cxx" line="119"/>
        <location filename="../Qt/ApplicationComponents/pqUndoRedoReaction.cxx" line="121"/>
        <source>Can&apos;t Undo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqUndoRedoReaction.cxx" line="119"/>
        <source>&amp;Undo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqUndoRedoReaction.cxx" line="121"/>
        <source>Undo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqUndoRedoReaction.cxx" line="125"/>
        <location filename="../Qt/ApplicationComponents/pqUndoRedoReaction.cxx" line="127"/>
        <source>Can&apos;t Redo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqUndoRedoReaction.cxx" line="125"/>
        <source>&amp;Redo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqUndoRedoReaction.cxx" line="127"/>
        <source>Redo</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqVCRToolbar</name>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqVCRToolbar.ui" line="6"/>
        <source>VCR Controls</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqVCRToolbar.ui" line="25"/>
        <source>&amp;Play</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqVCRToolbar.ui" line="36"/>
        <source>&amp;Reverse</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqVCRToolbar.ui" line="48"/>
        <source>Pre&amp;vious Frame</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqVCRToolbar.ui" line="62"/>
        <source>&amp;First Frame</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqVCRToolbar.ui" line="73"/>
        <source>&amp;Next Frame</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqVCRToolbar.ui" line="84"/>
        <source>&amp;Last Frame</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqVCRToolbar.ui" line="98"/>
        <source>L&amp;oop</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqVCRToolbar.ui" line="101"/>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqVCRToolbar.ui" line="104"/>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqVCRToolbar.ui" line="107"/>
        <source>Loop</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqVCRToolbar.cxx" line="100"/>
        <source>First Frame (%1)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqVCRToolbar.cxx" line="101"/>
        <source>Last Frame (%1)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqVCRToolbar.cxx" line="108"/>
        <source>Reverse</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqVCRToolbar.cxx" line="108"/>
        <source>Play</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqVCRToolbar.cxx" line="118"/>
        <source>Pa&amp;use</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqValueWidget</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqSelectionQueryPropertyWidget.cxx" line="235"/>
        <source>value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqSelectionQueryPropertyWidget.cxx" line="239"/>
        <source>comma separated values</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqSelectionQueryPropertyWidget.cxx" line="252"/>
        <source>minimum</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqSelectionQueryPropertyWidget.cxx" line="256"/>
        <source>maximum</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqSelectionQueryPropertyWidget.cxx" line="258"/>
        <source>and</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqSelectionQueryPropertyWidget.cxx" line="277"/>
        <source>X coordinate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqSelectionQueryPropertyWidget.cxx" line="280"/>
        <source>Y coordinate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqSelectionQueryPropertyWidget.cxx" line="283"/>
        <source>Z coordinate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqSelectionQueryPropertyWidget.cxx" line="294"/>
        <source>within epsilon</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqViewMenuManager</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqViewMenuManager.cxx" line="92"/>
        <source>Toolbars</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqViewMenuManager.cxx" line="107"/>
        <source>Preview</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqViewMenuManager.cxx" line="111"/>
        <source>Full Screen</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqViewMenuManager.cxx" line="117"/>
        <source>Show Frame Decorations</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqViewMenuManager.cxx" line="126"/>
        <source>Toggle Lock Panels</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqViewMenuManager.cxx" line="128"/>
        <source>Toggle locking of dockable panels so they    cannot be moved</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqViewMenuManager.cxx" line="132"/>
        <source>Rearrange Views</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqViewMenuManager.cxx" line="134"/>
        <source>Horizontally</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqViewMenuManager.cxx" line="137"/>
        <source>Rearrange layout so views are evenly sized horizontally</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqViewMenuManager.cxx" line="141"/>
        <source>Vertically</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqViewMenuManager.cxx" line="144"/>
        <source>Rearrange layout so views are evenly sized vertically</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqViewMenuManager.cxx" line="148"/>
        <source>Grid</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqViewMenuManager.cxx" line="151"/>
        <source>Rearrange layout so views are evenly sized horizontally and vertically</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqViewResolutionPropertyWidget</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqViewResolutionPropertyWidget.cxx" line="125"/>
        <source>Presets</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqViewTypePropertyWidget</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqViewTypePropertyWidget.cxx" line="56"/>
        <source>None</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqViewTypePropertyWidget.cxx" line="57"/>
        <source>Empty</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqWelcomeDialog</name>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqWelcomeDialog.ui" line="14"/>
        <source>Welcome to ParaView</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqWelcomeDialog.ui" line="59"/>
        <source>Example Visualizations</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqWelcomeDialog.ui" line="89"/>
        <source>Getting Started Guide</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqWelcomeDialog.ui" line="108"/>
        <source>Don&apos;t show this window again</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqmacrosToolbar</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="894"/>
        <source>Macros Toolbars</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
