<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1">
<context>
    <name>QuickLaunchDialog</name>
    <message>
        <location filename="../Qt/Widgets/Resources/UI/pqQuickLaunchDialog.ui" line="14"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Widgets/Resources/UI/pqQuickLaunchDialog.ui" line="52"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Type to search. &lt;span style=&quot; font-weight:600; font-style:italic;&quot;&gt;Enter&lt;/span&gt; to create selected source/filter. &lt;span style=&quot; font-weight:600; font-style:italic;&quot;&gt;Shift + Enter&lt;/span&gt; to create and apply selected source/filter. &lt;span style=&quot; font-weight:600; font-style:italic;&quot;&gt;Esc &lt;/span&gt;to cancel.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SeriesGeneratorDialog</name>
    <message>
        <location filename="../Qt/Widgets/Resources/UI/pqSeriesGeneratorDialog.ui" line="14"/>
        <source>Generate Number Series</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Widgets/Resources/UI/pqSeriesGeneratorDialog.ui" line="21"/>
        <source>Linear</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Widgets/Resources/UI/pqSeriesGeneratorDialog.ui" line="26"/>
        <source>Logarithmic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Widgets/Resources/UI/pqSeriesGeneratorDialog.ui" line="31"/>
        <source>Geometric (samples)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Widgets/Resources/UI/pqSeriesGeneratorDialog.ui" line="36"/>
        <source>Geometric (common ratio)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Widgets/Resources/UI/pqSeriesGeneratorDialog.ui" line="56"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;-&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Widgets/Resources/UI/pqSeriesGeneratorDialog.ui" line="73"/>
        <source>Reset using current data range values</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Widgets/Resources/UI/pqSeriesGeneratorDialog.ui" line="82"/>
        <source>Type:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Widgets/Resources/UI/pqSeriesGeneratorDialog.ui" line="89"/>
        <source>Number of Samples:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Widgets/Resources/UI/pqSeriesGeneratorDialog.ui" line="109"/>
        <source>Common Ratio:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Widgets/Resources/UI/pqSeriesGeneratorDialog.ui" line="116"/>
        <source>1.1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Widgets/Resources/UI/pqSeriesGeneratorDialog.ui" line="138"/>
        <source>Range</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqColorChooserButton</name>
    <message>
        <location filename="../Qt/Widgets/pqColorChooserButton.cxx" line="184"/>
        <source>Set Color</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqExpanderButton</name>
    <message>
        <location filename="../Qt/Widgets/Resources/UI/pqExpanderButton.ui" line="66"/>
        <source>Properties</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqProgressWidget</name>
    <message>
        <location filename="../Qt/Widgets/pqProgressWidget.cxx" line="142"/>
        <source>Abort</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqScaleByButton</name>
    <message>
        <location filename="../Qt/Widgets/pqScaleByButton.cxx" line="74"/>
        <source>Scale by ...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqSeriesGeneratorDialog</name>
    <message>
        <location filename="../Qt/Widgets/pqSeriesGeneratorDialog.cxx" line="84"/>
        <source>Error: range cannot contain 0 for log.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Widgets/pqSeriesGeneratorDialog.cxx" line="91"/>
        <source>Error: range cannot begin or end with 0 for a geometric series.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Widgets/pqSeriesGeneratorDialog.cxx" line="95"/>
        <source>Error: range cannot contain 0 for a geometric series.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Widgets/pqSeriesGeneratorDialog.cxx" line="102"/>
        <source>Error: range cannot begin with 0 for a geometric series.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Widgets/pqSeriesGeneratorDialog.cxx" line="106"/>
        <source>Error: common ratio cannot be 0 for a geometric series.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Widgets/pqSeriesGeneratorDialog.cxx" line="121"/>
        <source>Sample series: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Widgets/pqSeriesGeneratorDialog.cxx" line="219"/>
        <source>&amp;Generate</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqTreeViewSelectionHelper</name>
    <message>
        <location filename="../Qt/Widgets/pqTreeViewSelectionHelper.cxx" line="182"/>
        <source>Filter items (regex)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Widgets/pqTreeViewSelectionHelper.cxx" line="209"/>
        <source>Check highlighted items</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Widgets/pqTreeViewSelectionHelper.cxx" line="217"/>
        <source>Uncheck highlighted items</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Widgets/pqTreeViewSelectionHelper.cxx" line="239"/>
        <source>Sort (ascending)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Widgets/pqTreeViewSelectionHelper.cxx" line="240"/>
        <source>Sort (descending)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Widgets/pqTreeViewSelectionHelper.cxx" line="250"/>
        <source>Clear sorting</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqTreeWidgetSelectionHelper</name>
    <message>
        <location filename="../Qt/Widgets/pqTreeWidgetSelectionHelper.cxx" line="94"/>
        <source>Check</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Widgets/pqTreeWidgetSelectionHelper.cxx" line="95"/>
        <source>Uncheck</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
