cmake_minimum_required(VERSION 3.0)
project(ParaViewTranslations NONE)

find_package(Qt5 REQUIRED QUIET COMPONENTS Core LinguistTools)

set(INSTALL_SUBDIR "share/translations" CACHE STRING "Destination of the 
  result files within the INSTALL_PREFIX directory")
option(PROVIDE_QT_TRANSLATIONS "Build and install official Qt translation files" OFF)

if (PROVIDE_QT_TRANSLATIONS)
  set(qt_ts_prefix
    qt
    qtbase
    qtscript
    qtmultimedia
    qtxmlpatterns)
endif ()

set(ts_files
  Clients_ParaView.ts
  Qt_ApplicationComponents.ts
  Qt_Core.ts
  Qt_Widgets.ts
  Clients_ParaView-XMLs.ts
  Qt_Components.ts
  Qt_Python.ts
  ServerManager-XMLs.ts
  )

set(languages
  en_US
  fr_FR
  )

add_custom_target("files_update")

foreach (output_language IN LISTS languages)
  if (PROVIDE_QT_TRANSLATIONS)
    set(_qt_ts_language_prefix ${qt_ts_prefix})
    # Get the language code without the country code
    string(REGEX MATCH "^([a-z]+)"
      _language_code "${output_language}")
    list(TRANSFORM _qt_ts_language_prefix APPEND "_${_language_code}")
    foreach (_qt_ts IN LISTS _qt_ts_language_prefix)
      if ("${output_language}" STREQUAL "en_US")
        # Skip en_US as qt does not provide it.
        continue()
      endif()
      # Create qm generation targets
      set(destination_qt_qm "${CMAKE_BINARY_DIR}/${_qt_ts}.qm")
      set(source_ts "${CMAKE_SOURCE_DIR}/qttranslations/translations/${_qt_ts}.ts")
      add_custom_command(
        OUTPUT "${destination_qt_qm}"
        COMMAND "$<TARGET_FILE:Qt5::lconvert>" ${source_ts} -o "${destination_qt_qm}"
        DEPENDS "${source_ts}")
      add_custom_target("${_language_code}_${_qt_ts}" ALL DEPENDS "${destination_qt_qm}")
      install(
        FILES "${destination_qt_qm}"
        DESTINATION "${INSTALL_SUBDIR}"
      )
    endforeach ()
  endif ()

  set(ts_language_files ${ts_files})
  list(TRANSFORM ts_language_files PREPEND "${CMAKE_SOURCE_DIR}/${output_language}/")
  # Create qm generation targets
  set(destination_qm "${CMAKE_BINARY_DIR}/paraview_${output_language}.qm")
  add_custom_command(
    OUTPUT "${destination_qm}"
    COMMAND "$<TARGET_FILE:Qt5::lconvert>" ${ts_language_files} -o "${destination_qm}"
    DEPENDS ${ts_language_files})
  add_custom_target("${output_language}" ALL DEPENDS "${destination_qm}")
  if ("${output_language}" STREQUAL "en_US")
    # en_US files will never be used by ParaView
    continue()
  endif()
  install(
    FILES "${destination_qm}"
    DESTINATION "${INSTALL_SUBDIR}"
  )

  # Create ts files update target
  if ("${output_language}" STREQUAL "en_US")
    # Skip en_US as they are the template files
    continue()
  endif()
  add_custom_target("${output_language}_update")
  foreach (ts_file IN LISTS ts_files)
    set(absolute_ts_file "${CMAKE_SOURCE_DIR}/${output_language}/${ts_file}")
    set(absolute_en_file "${CMAKE_SOURCE_DIR}/en_US/${ts_file}")
    add_custom_target("${output_language}_${ts_file}_update")
    add_custom_command(
      TARGET "${output_language}_${ts_file}_update"
      COMMAND "$<TARGET_FILE:Qt5::lupdate>" "${absolute_en_file}" -ts "${absolute_ts_file}"
      )
    add_dependencies("${output_language}_update" "${output_language}_${ts_file}_update")
  endforeach ()
  add_dependencies("files_update" "${output_language}_update")
endforeach ()
