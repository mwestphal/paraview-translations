# ParaView-Translations

This repository contains translation files for [ParaView][pvrepo].

Each folder contains translations for one language (the name of the folder), except `en_US` who stores the template files with all source strings from [ParaView][pvrepo].

# Building

Translation files can be built using CMake:

```
cmake --build .
```

You can also build specific files with their specific locale name

In order to get the Japanese and French files, one should write:
```
cmake --build . --target ja_JP fr_FR
```

# License

This repository is under the Apache 2.0 license, see NOTICE and LICENSE file.

[pvrepo]: https://gitlab.kitware.com/paraview/paraview
